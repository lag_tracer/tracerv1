
# coding: utf-8

# In[1]:

from netCDF4 import Dataset
import ctypes as C 
import matplotlib.pyplot as plt
import matplotlib.animation as animation
import matplotlib.patches as patches
from pylab import*
import numpy as np
import scipy.interpolate as interpolate
import sys
from mpl_toolkits.mplot3d import Axes3D

c_char_p_p = C.POINTER(C.c_char_p)
MAXSTRLEN=100
c_char_array = C.ARRAY(C.c_char,MAXSTRLEN)
c_char_array_p = C.POINTER(c_char_array)

# # Extract TNTI Interface
# 
# ## 2D plane -> TNTI Interfaces

# In[2]:
cpus=12

#Input Variables
input_folder=b"/mnt/md0/PJ3000/PJ3000_Inst3D/" #In bytes literal representation
input_filehead=b"PJ_Inst3D_UVWP"

fileflag=2 #0 for U, V, W, E #1 for UVWP #2 for netcdf


input_lib_folder='lib/'
input_lib_name='pytracer_v1.so'

input_ensnorm=b"/mnt/md0/fortran_subroutines/ens_max_mean_std3000.txt"

gridy_filename="yp.dat"


#Arbitary number
sample_number=101

#Tracking
Tstart=280020
output_prefix='./'
Re=3000
nu=1./Re

#Filenumbers
initial_time=1
Nprocess=100
Tinterval=30

simulation_dt=0.004
dt=simulation_dt*Tinterval
taup=0.0
g=0
Rinit=0.1

ssflag=0
v0flag=1
ssnorm=1.1

#Subdomain dimension
nx=151
ny=1051
nz=300

dx=40./1501.
dy=40./1501.
dz=8./300.

subdomain_startX=375
subdomain_startY=225
subdomain_startZ=0

#Seeding box
xstart=10; xend=12; xinterval=100
ystart=20; yend=30; yinterval=100
zstart=0; zend=8; zinterval=100


grid_x=np.empty((nx),dtype=np.float32,order='F')
grid_y=np.empty((ny),dtype=np.float32,order='F')
grid_z=np.empty((nz),dtype=np.float32,order='F')

for i in range(nx):
  grid_x[i]=dx*(subdomain_startX+i)

print 'Use non uniform mesh on y? Yes=1 No=0'
flag=int(raw_input())
if flag==1:
  grid_y=np.genfromtxt(gridy_filename)
  grid_y=np.array(grid_y,dtype=np.float32,order='F')

  print grid_y.shape
  if (len(grid_y)!=ny):
    print 'Grid y has different dimension from ny', len(grid_y),ny
    stop
elif flag==0:
  print 'Use Uniform Grid'
  for i in range(ny):
    grid_y[i]=dy*(subdomain_startY+i)
else:
  print 'Read y grid failed'
  stop

for i in range(nz):
  grid_z[i]=dz*(subdomain_startZ+i)

#input_folder=C.create_string_buffer(input_folder,MAXSTRLEN)
#input_filehead=C.create_string_buffer(input_filehead,MAXSTRLEN)
#input_ensnorm=C.create_string_buffer(input_ensnorm,MAXSTRLEN)
################# read grids #####################
list_x=np.empty(())
list_y=np.empty(())
list_z=np.empty(())

list_x=np.linspace(xstart,xend,xinterval)
list_y=np.linspace(ystart,yend,yinterval)
list_z=np.linspace(zstart,zend,zinterval)

input01=np.array([[list_x[i], list_y[j],list_z[k]] for i in range(len(list_x)) for j in range(len(list_y)) for k in range(len(list_z))],dtype=np.float32,order='F')

print 'xmin',np.min(input01[:,0])
print 'xmax',np.max(input01[:,0])
print 'ymin',np.min(input01[:,1])
print 'ymax',np.max(input01[:,1])
print 'zmin',np.min(input01[:,2])
print 'zmax',np.max(input01[:,2])

print input01

Npoints=input01.shape[0]
print 'Number of points ', Npoints


# In[5]:


#Load Library find_point
tracer= np.ctypeslib.load_library(input_lib_name,input_lib_folder)

#specify argument types
tracer.pytrace_v1_.argtypes = [
               C.POINTER(C.c_int), #cpus
               C.POINTER(C.c_int), #n
               C.POINTER(C.c_int), #nx
               C.POINTER(C.c_int), #ny
               C.POINTER(C.c_int), #nz
               C.POINTER(C.c_float), #nu
               C.POINTER(C.c_float), #taup
               C.POINTER(C.c_float), #g
               C.POINTER(C.c_float), #Rinit
               C.POINTER(C.c_int), #ssflag
               C.POINTER(C.c_int), #v0flag
               C.POINTER(C.c_float), #ssnorm
               np.ctypeslib.ndpointer(dtype=np.float32), #grid_x
               np.ctypeslib.ndpointer(dtype=np.float32), #grid_y
               np.ctypeslib.ndpointer(dtype=np.float32), #grid_z
               np.ctypeslib.ndpointer(dtype=np.float32), #point_input
               C.POINTER(C.c_int), #start_time
               C.POINTER(C.c_int), #process_files
               C.POINTER(C.c_int), #t_interval
               C.POINTER(C.c_float), #dt
               C.POINTER(C.c_int), #fileflag
               C.c_char_p, #input_folder
               C.c_char_p, #input_filehead
               C.c_char_p, #input_filehead
               C.c_char_p, #output_filename
               C.c_int, C.c_int, C.c_int, C.c_int,]

tracer.pytrace_v1_.restype = C.c_void_p  


# In[ ]:


#You can make loops on different start time!
for t in range(0,1):
    time=Tstart#+t*3000
    output_filename='output_all_domain_sample'+str(sample_number)+'_'+str(time)+'_taup_'+str(taup)+'_g_'+str(g)+'.nc'

    output_filename=C.create_string_buffer(output_filename,MAXSTRLEN)

    print 'Python: Computing at time ', time 
    print 'Python: Output_filename ', output_filename

    tracer.pytrace_v1_(
                    C.c_int(cpus),
                    C.c_int(Npoints),
                    C.c_int(nx),
                    C.c_int(ny),
                    C.c_int(nz),
                    C.c_float(nu),
                    C.c_float(taup),
                    C.c_float(g),
                    C.c_float(Rinit),
                    C.c_int(ssflag),
                    C.c_int(v0flag),
                    C.c_float(ssnorm),
                    grid_x,
                    grid_y,
                    grid_z,
                    input01,
                    C.c_int(time),
                    C.c_int(Nprocess),
                    C.c_int(Tinterval),
                    C.c_float(dt),
                    C.c_int(fileflag),
                    input_folder,  
                    input_filehead,
                    input_ensnorm,
                    output_filename,
                    len(input_folder), len(input_filehead),
                    len(input_ensnorm), len(output_filename))
    print 'Python: Done!'

