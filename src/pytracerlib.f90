subroutine compute_ens(U,V,W,ENS,nx,ny,nz,dx,dy,dz)

!**********************************************************************
!Subroutine compute_vorticity
!Subroutine to compute vorticity of the field
!Input module Definition
!Output module Definition
!**********************************************************************
  
  !$ use omp_lib
  Implicit none  
  Integer i,j,k

  integer,intent(in)::nx,ny,nz
  real, dimension(nx,ny,nz)::U,V,W
  real, dimension(nx,ny,nz)::ENS
  real::dx,dy,dz

  double precision::dudx,dudy,dudz,dvdx,dvdy,dvdz,dwdx,dwdy,dwdz

!$OMP PARALLEL default(shared),private(i,j,k,dudx,dudy,dudz,dvdx,dvdy,dvdz,dwdx,dwdy,dwdz)
!$OMP DO
  do i=1,nx
    do j=1,ny
      do k=1,nz
        
          if (i==1) then
            dudx=(-3./2.)*U(i,j,k)+(2.)*U(i+1,j,k)+(-1./2.)*U(i+2,j,k)
            dvdx=(-3./2.)*V(i,j,k)+(2.)*V(i+1,j,k)+(-1./2.)*V(i+2,j,k)
            dwdx=(-3./2.)*W(i,j,k)+(2.)*W(i+1,j,k)+(-1./2.)*W(i+2,j,k)
          else if (i==2) then
            dudx=(-1./2.)*U(i-1,j,k)+(1./2.)*U(i+1,j,k)
            dvdx=(-1./2.)*V(i-1,j,k)+(1./2.)*V(i+1,j,k)
            dwdx=(-1./2.)*W(i-1,j,k)+(1./2.)*W(i+1,j,k)
          else if (i==nx-1) then
            dudx=(-1./2.)*U(i-1,j,k)+(1./2.)*U(i+1,j,k)
            dvdx=(-1./2.)*V(i-1,j,k)+(1./2.)*V(i+1,j,k)
            dwdx=(-1./2.)*W(i-1,j,k)+(1./2.)*W(i+1,j,k)
          else if (i==nx) then
            dudx=(3./2.)*U(i,j,k)+(-2.)*U(i-1,j,k)+(1./2.)*U(i-2,j,k)
            dvdx=(3./2.)*V(i,j,k)+(-2.)*V(i-1,j,k)+(1./2.)*V(i-2,j,k)
            dwdx=(3./2.)*W(i,j,k)+(-2.)*W(i-1,j,k)+(1./2.)*W(i-2,j,k)
          else
            dudx=(1./12.)*U(i-2,j,k)+(-2./3.)*U(i-1,j,k)+(2./3.)*U(i+1,j,k)+(-1./12.)*U(i+2,j,k)
            dvdx=(1./12.)*V(i-2,j,k)+(-2./3.)*V(i-1,j,k)+(2./3.)*V(i+1,j,k)+(-1./12.)*V(i+2,j,k)
            dwdx=(1./12.)*W(i-2,j,k)+(-2./3.)*W(i-1,j,k)+(2./3.)*W(i+1,j,k)+(-1./12.)*W(i+2,j,k)
          end if

          dudx=dudx/dx
          dvdx=dvdx/dx
          dwdx=dwdx/dx

          if (j==1) then
            dudy=(-3./2.)*U(i,j,k)+(2.)*U(i,j+1,k)+(-1./2.)*U(i,j+2,k)
            dvdy=(-3./2.)*V(i,j,k)+(2.)*V(i,j+1,k)+(-1./2.)*V(i,j+2,k)
            dwdy=(-3./2.)*W(i,j,k)+(2.)*W(i,j+1,k)+(-1./2.)*W(i,j+2,k)
          else if (j==2) then
            dudy=(-1./2.)*U(i,j-1,k)+(1./2.)*U(i,j+1,k)
            dvdy=(-1./2.)*V(i,j-1,k)+(1./2.)*V(i,j+1,k)
            dwdy=(-1./2.)*W(i,j-1,k)+(1./2.)*W(i,j+1,k)
          else if (j==ny-1) then
            dudy=(-1./2.)*U(i,j-1,k)+(1./2.)*U(i,j+1,k)
            dvdy=(-1./2.)*V(i,j-1,k)+(1./2.)*V(i,j+1,k)
            dwdy=(-1./2.)*W(i,j-1,k)+(1./2.)*W(i,j+1,k)
          else if (j==ny) then
            dudy=(3./2.)*U(i,j,k)+(-2.)*U(i,j-1,k)+(1./2.)*U(i,j-2,k)
            dvdy=(3./2.)*V(i,j,k)+(-2.)*V(i,j-1,k)+(1./2.)*V(i,j-2,k)
            dwdy=(3./2.)*W(i,j,k)+(-2.)*W(i,j-1,k)+(1./2.)*W(i,j-2,k)
          else
            dudy=(1./12.)*U(i,j-2,k)+(-2./3.)*U(i,j-1,k)+(2./3.)*U(i,j+1,k)+(-1./12.)*U(i,j+2,k)
            dvdy=(1./12.)*V(i,j-2,k)+(-2./3.)*V(i,j-1,k)+(2./3.)*V(i,j+1,k)+(-1./12.)*V(i,j+2,k)
            dwdy=(1./12.)*W(i,j-2,k)+(-2./3.)*W(i,j-1,k)+(2./3.)*W(i,j+1,k)+(-1./12.)*W(i,j+2,k)
          end if
          dudy=dudy/dy
          dvdy=dvdy/dy
          dwdy=dwdy/dy

          if (k==1) then
            dudz=(-3./2.)*U(i,j,k)+(2.)*U(i,j,k+1)+(-1./2.)*U(i,j,k+2)
            dvdz=(-3./2.)*V(i,j,k)+(2.)*V(i,j,k+1)+(-1./2.)*V(i,j,k+2)
            dwdz=(-3./2.)*W(i,j,k)+(2.)*W(i,j,k+1)+(-1./2.)*W(i,j,k+2)
          else if (k==2) then
            dudz=(-1./2.)*U(i,j,k-1)+(1./2.)*U(i,j,k+1)
            dvdz=(-1./2.)*V(i,j,k-1)+(1./2.)*V(i,j,k+1)
            dwdz=(-1./2.)*W(i,j,k-1)+(1./2.)*W(i,j,k+1)
          else if (k==nz-1) then
            dudz=(-1./2.)*U(i,j,k-1)+(1./2.)*U(i,j,k+1)
            dvdz=(-1./2.)*V(i,j,k-1)+(1./2.)*V(i,j,k+1)
            dwdz=(-1./2.)*W(i,j,k-1)+(1./2.)*W(i,j,k+1)
          else if (k==nz) then
            dudz=(3./2.)*U(i,j,k)+(-2.)*U(i,j,k-1)+(1./2.)*U(i,j,k-2)
            dvdz=(3./2.)*V(i,j,k)+(-2.)*V(i,j,k-1)+(1./2.)*V(i,j,k-2)
            dwdz=(3./2.)*W(i,j,k)+(-2.)*W(i,j,k-1)+(1./2.)*W(i,j,k-2)
          else
            dudz=(1./12.)*U(i,j,k-2)+(-2./3.)*U(i,j,k-1)+(2./3.)*U(i,j,k+1)+(-1./12.)*U(i,j,k+2)
            dvdz=(1./12.)*V(i,j,k-2)+(-2./3.)*V(i,j,k-1)+(2./3.)*V(i,j,k+1)+(-1./12.)*V(i,j,k+2)
            dwdz=(1./12.)*W(i,j,k-2)+(-2./3.)*W(i,j,k-1)+(2./3.)*W(i,j,k+1)+(-1./12.)*W(i,j,k+2)
          end if
          dudz=dudz/dz
          dvdz=dvdz/dz
          dwdz=dwdz/dz

        ENS(i,j,k)=(dwdy-dvdz)**2+(dudz-dwdx)**2+(dvdx-dudy)**2

      end do
    end do
  end do
!$OMP END DO
!$OMP END PARALLEL

  

  return
End subroutine

subroutine compute_vorticity(U,V,W,s11,s22,s33,s12,s13,s23,VOR_x,VOR_y,VOR_z,xpmin,xpmax,ypmin,ypmax,zpmin,zpmax,dx,dy,dz)

!**********************************************************************
!Subroutine compute_vorticity
!Subroutine to compute vorticity of the field
!Input module Definition
!Output module Definition
!**********************************************************************
  
  !$ use omp_lib
  Implicit none  
  Integer i,j,k

  integer,intent(in)::xpmin,xpmax,ypmin,ypmax,zpmin,zpmax
  real, dimension(xpmin:xpmax,ypmin:ypmax,zpmin:zpmax)::U,V,W
  real, dimension(xpmin:xpmax,ypmin:ypmax,zpmin:zpmax)::VOR_x,VOR_y,VOR_z
  real, dimension(xpmin:xpmax,ypmin:ypmax,zpmin:zpmax)::s11,s22,s33,s12,s13,s23
  real::dx,dy,dz

  double precision::dudx,dudy,dudz,dvdx,dvdy,dvdz,dwdx,dwdy,dwdz

!$OMP PARALLEL default(shared),private(i,j,k,dudx,dudy,dudz,dvdx,dvdy,dvdz,dwdx,dwdy,dwdz)
!$OMP DO
  do i=xpmin,xpmax
    do j=ypmin,ypmax
      do k=zpmin,zpmax
        
          if (i==xpmin) then
            dudx=(-3./2.)*U(i,j,k)+(2.)*U(i+1,j,k)+(-1./2.)*U(i+2,j,k)
            dvdx=(-3./2.)*V(i,j,k)+(2.)*V(i+1,j,k)+(-1./2.)*V(i+2,j,k)
            dwdx=(-3./2.)*W(i,j,k)+(2.)*W(i+1,j,k)+(-1./2.)*W(i+2,j,k)
          else if (i==xpmin+1) then
            dudx=(-1./2.)*U(i-1,j,k)+(1./2.)*U(i+1,j,k)
            dvdx=(-1./2.)*V(i-1,j,k)+(1./2.)*V(i+1,j,k)
            dwdx=(-1./2.)*W(i-1,j,k)+(1./2.)*W(i+1,j,k)
          else if (i==xpmax-1) then
            dudx=(-1./2.)*U(i-1,j,k)+(1./2.)*U(i+1,j,k)
            dvdx=(-1./2.)*V(i-1,j,k)+(1./2.)*V(i+1,j,k)
            dwdx=(-1./2.)*W(i-1,j,k)+(1./2.)*W(i+1,j,k)
          else if (i==xpmax) then
            dudx=(3./2.)*U(i,j,k)+(-2.)*U(i-1,j,k)+(1./2.)*U(i-2,j,k)
            dvdx=(3./2.)*V(i,j,k)+(-2.)*V(i-1,j,k)+(1./2.)*V(i-2,j,k)
            dwdx=(3./2.)*W(i,j,k)+(-2.)*W(i-1,j,k)+(1./2.)*W(i-2,j,k)
          else
            dudx=(1./12.)*U(i-2,j,k)+(-2./3.)*U(i-1,j,k)+(2./3.)*U(i+1,j,k)+(-1./12.)*U(i+2,j,k)
            dvdx=(1./12.)*V(i-2,j,k)+(-2./3.)*V(i-1,j,k)+(2./3.)*V(i+1,j,k)+(-1./12.)*V(i+2,j,k)
            dwdx=(1./12.)*W(i-2,j,k)+(-2./3.)*W(i-1,j,k)+(2./3.)*W(i+1,j,k)+(-1./12.)*W(i+2,j,k)
          end if

          dudx=dudx/dx
          dvdx=dvdx/dx
          dwdx=dwdx/dx

          if (j==ypmin) then
            dudy=(-3./2.)*U(i,j,k)+(2.)*U(i,j+1,k)+(-1./2.)*U(i,j+2,k)
            dvdy=(-3./2.)*V(i,j,k)+(2.)*V(i,j+1,k)+(-1./2.)*V(i,j+2,k)
            dwdy=(-3./2.)*W(i,j,k)+(2.)*W(i,j+1,k)+(-1./2.)*W(i,j+2,k)
          else if (j==ypmin+1) then
            dudy=(-1./2.)*U(i,j-1,k)+(1./2.)*U(i,j+1,k)
            dvdy=(-1./2.)*V(i,j-1,k)+(1./2.)*V(i,j+1,k)
            dwdy=(-1./2.)*W(i,j-1,k)+(1./2.)*W(i,j+1,k)
          else if (j==ypmax-1) then
            dudy=(-1./2.)*U(i,j-1,k)+(1./2.)*U(i,j+1,k)
            dvdy=(-1./2.)*V(i,j-1,k)+(1./2.)*V(i,j+1,k)
            dwdy=(-1./2.)*W(i,j-1,k)+(1./2.)*W(i,j+1,k)
          else if (j==ypmax) then
            dudy=(3./2.)*U(i,j,k)+(-2.)*U(i,j-1,k)+(1./2.)*U(i,j-2,k)
            dvdy=(3./2.)*V(i,j,k)+(-2.)*V(i,j-1,k)+(1./2.)*V(i,j-2,k)
            dwdy=(3./2.)*W(i,j,k)+(-2.)*W(i,j-1,k)+(1./2.)*W(i,j-2,k)
          else
            dudy=(1./12.)*U(i,j-2,k)+(-2./3.)*U(i,j-1,k)+(2./3.)*U(i,j+1,k)+(-1./12.)*U(i,j+2,k)
            dvdy=(1./12.)*V(i,j-2,k)+(-2./3.)*V(i,j-1,k)+(2./3.)*V(i,j+1,k)+(-1./12.)*V(i,j+2,k)
            dwdy=(1./12.)*W(i,j-2,k)+(-2./3.)*W(i,j-1,k)+(2./3.)*W(i,j+1,k)+(-1./12.)*W(i,j+2,k)
          end if
          dudy=dudy/dy
          dvdy=dvdy/dy
          dwdy=dwdy/dy

          if (k==zpmin) then
            dudz=(-3./2.)*U(i,j,k)+(2.)*U(i,j,k+1)+(-1./2.)*U(i,j,k+2)
            dvdz=(-3./2.)*V(i,j,k)+(2.)*V(i,j,k+1)+(-1./2.)*V(i,j,k+2)
            dwdz=(-3./2.)*W(i,j,k)+(2.)*W(i,j,k+1)+(-1./2.)*W(i,j,k+2)
          else if (k==zpmin+1) then
            dudz=(-1./2.)*U(i,j,k-1)+(1./2.)*U(i,j,k+1)
            dvdz=(-1./2.)*V(i,j,k-1)+(1./2.)*V(i,j,k+1)
            dwdz=(-1./2.)*W(i,j,k-1)+(1./2.)*W(i,j,k+1)
          else if (k==zpmax-1) then
            dudz=(-1./2.)*U(i,j,k-1)+(1./2.)*U(i,j,k+1)
            dvdz=(-1./2.)*V(i,j,k-1)+(1./2.)*V(i,j,k+1)
            dwdz=(-1./2.)*W(i,j,k-1)+(1./2.)*W(i,j,k+1)
          else if (k==zpmax) then
            dudz=(3./2.)*U(i,j,k)+(-2.)*U(i,j,k-1)+(1./2.)*U(i,j,k-2)
            dvdz=(3./2.)*V(i,j,k)+(-2.)*V(i,j,k-1)+(1./2.)*V(i,j,k-2)
            dwdz=(3./2.)*W(i,j,k)+(-2.)*W(i,j,k-1)+(1./2.)*W(i,j,k-2)
          else
            dudz=(1./12.)*U(i,j,k-2)+(-2./3.)*U(i,j,k-1)+(2./3.)*U(i,j,k+1)+(-1./12.)*U(i,j,k+2)
            dvdz=(1./12.)*V(i,j,k-2)+(-2./3.)*V(i,j,k-1)+(2./3.)*V(i,j,k+1)+(-1./12.)*V(i,j,k+2)
            dwdz=(1./12.)*W(i,j,k-2)+(-2./3.)*W(i,j,k-1)+(2./3.)*W(i,j,k+1)+(-1./12.)*W(i,j,k+2)
          end if
          dudz=dudz/dz
          dvdz=dvdz/dz
          dwdz=dwdz/dz

        s11(i,j,k)=dudx
        s22(i,j,k)=dvdy
        s33(i,j,k)=dwdz
        s12(i,j,k)=0.5*(dudy+dvdx)
        s13(i,j,k)=0.5*(dudz+dwdx)
        s23(i,j,k)=0.5*(dvdz+dwdy)

        VOR_x(i,j,k)=dwdy-dvdz
        VOR_y(i,j,k)=dudz-dwdx
        VOR_z(i,j,k)=dvdx-dudy

        !VOR(i,j,k)=VOR_x(i,j,k)**2+VOR_y(i,j,k)**2+VOR_z(i,j,k)**2
        !DIS(i,j,k)=s11(i,j,k)**2+s22(i,j,k)**2+s33(i,j,k)**2+2.*s12(i,j,k)**2+2.*s13(i,j,k)**2+2.*s23(i,j,k)**2

      end do
    end do
  end do
!$OMP END DO
!$OMP END PARALLEL

  

  return
End subroutine


subroutine compute_vort4000(U,V,W,VOR,DIS,xpmin,xpmax,ypmin,ypmax,zpmin,zpmax,dx,dy,dz)

!**********************************************************************
!Subroutine compute_vorticity
!Subroutine to compute vorticity of the field
!Input module Definition
!Output module Definition
!**********************************************************************
  
  !$ use omp_lib
  Implicit none  
  Integer i,j,k

  integer,intent(in)::xpmin,xpmax,ypmin,ypmax,zpmin,zpmax
  real, dimension(xpmin:xpmax,ypmin:ypmax,zpmin:zpmax)::U,V,W
  real, dimension(xpmin:xpmax,ypmin:ypmax,zpmin:zpmax)::VOR_x,VOR_y,VOR_z,VOR,DIS
  real, dimension(xpmin:xpmax,ypmin:ypmax,zpmin:zpmax)::s11,s22,s33,s12,s13,s23
  real::dx,dy,dz

  double precision::dudx,dudy,dudz,dvdx,dvdy,dvdz,dwdx,dwdy,dwdz

!$OMP PARALLEL default(shared),private(i,j,k,dudx,dudy,dudz,dvdx,dvdy,dvdz,dwdx,dwdy,dwdz)
!$OMP DO
  do i=xpmin,xpmax
    do j=ypmin,ypmax
      do k=zpmin,zpmax
        
          if (i==xpmin) then
            dudx=(-3./2.)*U(i,j,k)+(2.)*U(i+1,j,k)+(-1./2.)*U(i+2,j,k)
            dvdx=(-3./2.)*V(i,j,k)+(2.)*V(i+1,j,k)+(-1./2.)*V(i+2,j,k)
            dwdx=(-3./2.)*W(i,j,k)+(2.)*W(i+1,j,k)+(-1./2.)*W(i+2,j,k)
          else if (i==xpmin+1) then
            dudx=(-1./2.)*U(i-1,j,k)+(1./2.)*U(i+1,j,k)
            dvdx=(-1./2.)*V(i-1,j,k)+(1./2.)*V(i+1,j,k)
            dwdx=(-1./2.)*W(i-1,j,k)+(1./2.)*W(i+1,j,k)
          else if (i==xpmax-1) then
            dudx=(-1./2.)*U(i-1,j,k)+(1./2.)*U(i+1,j,k)
            dvdx=(-1./2.)*V(i-1,j,k)+(1./2.)*V(i+1,j,k)
            dwdx=(-1./2.)*W(i-1,j,k)+(1./2.)*W(i+1,j,k)
          else if (i==xpmax) then
            dudx=(3./2.)*U(i,j,k)+(-2.)*U(i-1,j,k)+(1./2.)*U(i-2,j,k)
            dvdx=(3./2.)*V(i,j,k)+(-2.)*V(i-1,j,k)+(1./2.)*V(i-2,j,k)
            dwdx=(3./2.)*W(i,j,k)+(-2.)*W(i-1,j,k)+(1./2.)*W(i-2,j,k)
          else
            dudx=(1./12.)*U(i-2,j,k)+(-2./3.)*U(i-1,j,k)+(2./3.)*U(i+1,j,k)+(-1./12.)*U(i+2,j,k)
            dvdx=(1./12.)*V(i-2,j,k)+(-2./3.)*V(i-1,j,k)+(2./3.)*V(i+1,j,k)+(-1./12.)*V(i+2,j,k)
            dwdx=(1./12.)*W(i-2,j,k)+(-2./3.)*W(i-1,j,k)+(2./3.)*W(i+1,j,k)+(-1./12.)*W(i+2,j,k)
          end if

          dudx=dudx/dx
          dvdx=dvdx/dx
          dwdx=dwdx/dx

          if (j==ypmin) then
            dudy=(-3./2.)*U(i,j,k)+(2.)*U(i,j+1,k)+(-1./2.)*U(i,j+2,k)
            dvdy=(-3./2.)*V(i,j,k)+(2.)*V(i,j+1,k)+(-1./2.)*V(i,j+2,k)
            dwdy=(-3./2.)*W(i,j,k)+(2.)*W(i,j+1,k)+(-1./2.)*W(i,j+2,k)
          else if (j==ypmin+1) then
            dudy=(-1./2.)*U(i,j-1,k)+(1./2.)*U(i,j+1,k)
            dvdy=(-1./2.)*V(i,j-1,k)+(1./2.)*V(i,j+1,k)
            dwdy=(-1./2.)*W(i,j-1,k)+(1./2.)*W(i,j+1,k)
          else if (j==ypmax-1) then
            dudy=(-1./2.)*U(i,j-1,k)+(1./2.)*U(i,j+1,k)
            dvdy=(-1./2.)*V(i,j-1,k)+(1./2.)*V(i,j+1,k)
            dwdy=(-1./2.)*W(i,j-1,k)+(1./2.)*W(i,j+1,k)
          else if (j==ypmax) then
            dudy=(3./2.)*U(i,j,k)+(-2.)*U(i,j-1,k)+(1./2.)*U(i,j-2,k)
            dvdy=(3./2.)*V(i,j,k)+(-2.)*V(i,j-1,k)+(1./2.)*V(i,j-2,k)
            dwdy=(3./2.)*W(i,j,k)+(-2.)*W(i,j-1,k)+(1./2.)*W(i,j-2,k)
          else
            dudy=(1./12.)*U(i,j-2,k)+(-2./3.)*U(i,j-1,k)+(2./3.)*U(i,j+1,k)+(-1./12.)*U(i,j+2,k)
            dvdy=(1./12.)*V(i,j-2,k)+(-2./3.)*V(i,j-1,k)+(2./3.)*V(i,j+1,k)+(-1./12.)*V(i,j+2,k)
            dwdy=(1./12.)*W(i,j-2,k)+(-2./3.)*W(i,j-1,k)+(2./3.)*W(i,j+1,k)+(-1./12.)*W(i,j+2,k)
          end if
          dudy=dudy/dy
          dvdy=dvdy/dy
          dwdy=dwdy/dy

          if (k==zpmin) then
            dudz=(-3./2.)*U(i,j,k)+(2.)*U(i,j,k+1)+(-1./2.)*U(i,j,k+2)
            dvdz=(-3./2.)*V(i,j,k)+(2.)*V(i,j,k+1)+(-1./2.)*V(i,j,k+2)
            dwdz=(-3./2.)*W(i,j,k)+(2.)*W(i,j,k+1)+(-1./2.)*W(i,j,k+2)
          else if (k==zpmin+1) then
            dudz=(-1./2.)*U(i,j,k-1)+(1./2.)*U(i,j,k+1)
            dvdz=(-1./2.)*V(i,j,k-1)+(1./2.)*V(i,j,k+1)
            dwdz=(-1./2.)*W(i,j,k-1)+(1./2.)*W(i,j,k+1)
          else if (k==zpmax-1) then
            dudz=(-1./2.)*U(i,j,k-1)+(1./2.)*U(i,j,k+1)
            dvdz=(-1./2.)*V(i,j,k-1)+(1./2.)*V(i,j,k+1)
            dwdz=(-1./2.)*W(i,j,k-1)+(1./2.)*W(i,j,k+1)
          else if (k==zpmax) then
            dudz=(3./2.)*U(i,j,k)+(-2.)*U(i,j,k-1)+(1./2.)*U(i,j,k-2)
            dvdz=(3./2.)*V(i,j,k)+(-2.)*V(i,j,k-1)+(1./2.)*V(i,j,k-2)
            dwdz=(3./2.)*W(i,j,k)+(-2.)*W(i,j,k-1)+(1./2.)*W(i,j,k-2)
          else
            dudz=(1./12.)*U(i,j,k-2)+(-2./3.)*U(i,j,k-1)+(2./3.)*U(i,j,k+1)+(-1./12.)*U(i,j,k+2)
            dvdz=(1./12.)*V(i,j,k-2)+(-2./3.)*V(i,j,k-1)+(2./3.)*V(i,j,k+1)+(-1./12.)*V(i,j,k+2)
            dwdz=(1./12.)*W(i,j,k-2)+(-2./3.)*W(i,j,k-1)+(2./3.)*W(i,j,k+1)+(-1./12.)*W(i,j,k+2)
          end if
          dudz=dudz/dz
          dvdz=dvdz/dz
          dwdz=dwdz/dz

        s11(i,j,k)=dudx
        s22(i,j,k)=dvdy
        s33(i,j,k)=dwdz
        s12(i,j,k)=0.5*(dudy+dvdx)
        s13(i,j,k)=0.5*(dudz+dwdx)
        s23(i,j,k)=0.5*(dvdz+dwdy)

        VOR_x(i,j,k)=dwdy-dvdz
        VOR_y(i,j,k)=dudz-dwdx
        VOR_z(i,j,k)=dvdx-dudy

        VOR(i,j,k)=sqrt(VOR_x(i,j,k)**2+VOR_y(i,j,k)**2+VOR_z(i,j,k)**2)
        DIS(i,j,k)=s11(i,j,k)**2+s22(i,j,k)**2+s33(i,j,k)**2+2.*s12(i,j,k)**2+2.*s13(i,j,k)**2+2.*s23(i,j,k)**2

      end do
    end do
  end do
!$OMP END DO
!$OMP END PARALLEL

  

  return
End subroutine

subroutine compute_vorticity4000(U,V,W,s11,s22,s33,s12,s13,s23,VOR_x,VOR_y,VOR_z,VOR,DIS,&
                                                  xpmin,xpmax,ypmin,ypmax,zpmin,zpmax,dx,dy,dz)

!**********************************************************************
!Subroutine compute_vorticity
!Subroutine to compute vorticity of the field
!Input module Definition
!Output module Definition
!**********************************************************************
  
  !$ use omp_lib
  Implicit none  
  Integer i,j,k

  integer,intent(in)::xpmin,xpmax,ypmin,ypmax,zpmin,zpmax
  real, dimension(xpmin:xpmax,ypmin:ypmax,zpmin:zpmax)::U,V,W
  real, dimension(xpmin:xpmax,ypmin:ypmax,zpmin:zpmax)::VOR_x,VOR_y,VOR_z,VOR,DIS
  real, dimension(xpmin:xpmax,ypmin:ypmax,zpmin:zpmax)::s11,s22,s33,s12,s13,s23
  real::dx,dy,dz

  double precision::dudx,dudy,dudz,dvdx,dvdy,dvdz,dwdx,dwdy,dwdz

!$OMP PARALLEL default(shared),private(i,j,k,dudx,dudy,dudz,dvdx,dvdy,dvdz,dwdx,dwdy,dwdz)
!$OMP DO
  do i=xpmin,xpmax
    do j=ypmin,ypmax
      do k=zpmin,zpmax
        
          if (i==xpmin) then
            dudx=(-3./2.)*U(i,j,k)+(2.)*U(i+1,j,k)+(-1./2.)*U(i+2,j,k)
            dvdx=(-3./2.)*V(i,j,k)+(2.)*V(i+1,j,k)+(-1./2.)*V(i+2,j,k)
            dwdx=(-3./2.)*W(i,j,k)+(2.)*W(i+1,j,k)+(-1./2.)*W(i+2,j,k)
          else if (i==xpmin+1) then
            dudx=(-1./2.)*U(i-1,j,k)+(1./2.)*U(i+1,j,k)
            dvdx=(-1./2.)*V(i-1,j,k)+(1./2.)*V(i+1,j,k)
            dwdx=(-1./2.)*W(i-1,j,k)+(1./2.)*W(i+1,j,k)
          else if (i==xpmax-1) then
            dudx=(-1./2.)*U(i-1,j,k)+(1./2.)*U(i+1,j,k)
            dvdx=(-1./2.)*V(i-1,j,k)+(1./2.)*V(i+1,j,k)
            dwdx=(-1./2.)*W(i-1,j,k)+(1./2.)*W(i+1,j,k)
          else if (i==xpmax) then
            dudx=(3./2.)*U(i,j,k)+(-2.)*U(i-1,j,k)+(1./2.)*U(i-2,j,k)
            dvdx=(3./2.)*V(i,j,k)+(-2.)*V(i-1,j,k)+(1./2.)*V(i-2,j,k)
            dwdx=(3./2.)*W(i,j,k)+(-2.)*W(i-1,j,k)+(1./2.)*W(i-2,j,k)
          else
            dudx=(1./12.)*U(i-2,j,k)+(-2./3.)*U(i-1,j,k)+(2./3.)*U(i+1,j,k)+(-1./12.)*U(i+2,j,k)
            dvdx=(1./12.)*V(i-2,j,k)+(-2./3.)*V(i-1,j,k)+(2./3.)*V(i+1,j,k)+(-1./12.)*V(i+2,j,k)
            dwdx=(1./12.)*W(i-2,j,k)+(-2./3.)*W(i-1,j,k)+(2./3.)*W(i+1,j,k)+(-1./12.)*W(i+2,j,k)
          end if

          dudx=dudx/dx
          dvdx=dvdx/dx
          dwdx=dwdx/dx

          if (j==ypmin) then
            dudy=(-3./2.)*U(i,j,k)+(2.)*U(i,j+1,k)+(-1./2.)*U(i,j+2,k)
            dvdy=(-3./2.)*V(i,j,k)+(2.)*V(i,j+1,k)+(-1./2.)*V(i,j+2,k)
            dwdy=(-3./2.)*W(i,j,k)+(2.)*W(i,j+1,k)+(-1./2.)*W(i,j+2,k)
          else if (j==ypmin+1) then
            dudy=(-1./2.)*U(i,j-1,k)+(1./2.)*U(i,j+1,k)
            dvdy=(-1./2.)*V(i,j-1,k)+(1./2.)*V(i,j+1,k)
            dwdy=(-1./2.)*W(i,j-1,k)+(1./2.)*W(i,j+1,k)
          else if (j==ypmax-1) then
            dudy=(-1./2.)*U(i,j-1,k)+(1./2.)*U(i,j+1,k)
            dvdy=(-1./2.)*V(i,j-1,k)+(1./2.)*V(i,j+1,k)
            dwdy=(-1./2.)*W(i,j-1,k)+(1./2.)*W(i,j+1,k)
          else if (j==ypmax) then
            dudy=(3./2.)*U(i,j,k)+(-2.)*U(i,j-1,k)+(1./2.)*U(i,j-2,k)
            dvdy=(3./2.)*V(i,j,k)+(-2.)*V(i,j-1,k)+(1./2.)*V(i,j-2,k)
            dwdy=(3./2.)*W(i,j,k)+(-2.)*W(i,j-1,k)+(1./2.)*W(i,j-2,k)
          else
            dudy=(1./12.)*U(i,j-2,k)+(-2./3.)*U(i,j-1,k)+(2./3.)*U(i,j+1,k)+(-1./12.)*U(i,j+2,k)
            dvdy=(1./12.)*V(i,j-2,k)+(-2./3.)*V(i,j-1,k)+(2./3.)*V(i,j+1,k)+(-1./12.)*V(i,j+2,k)
            dwdy=(1./12.)*W(i,j-2,k)+(-2./3.)*W(i,j-1,k)+(2./3.)*W(i,j+1,k)+(-1./12.)*W(i,j+2,k)
          end if
          dudy=dudy/dy
          dvdy=dvdy/dy
          dwdy=dwdy/dy

          if (k==zpmin) then
            dudz=(-3./2.)*U(i,j,k)+(2.)*U(i,j,k+1)+(-1./2.)*U(i,j,k+2)
            dvdz=(-3./2.)*V(i,j,k)+(2.)*V(i,j,k+1)+(-1./2.)*V(i,j,k+2)
            dwdz=(-3./2.)*W(i,j,k)+(2.)*W(i,j,k+1)+(-1./2.)*W(i,j,k+2)
          else if (k==zpmin+1) then
            dudz=(-1./2.)*U(i,j,k-1)+(1./2.)*U(i,j,k+1)
            dvdz=(-1./2.)*V(i,j,k-1)+(1./2.)*V(i,j,k+1)
            dwdz=(-1./2.)*W(i,j,k-1)+(1./2.)*W(i,j,k+1)
          else if (k==zpmax-1) then
            dudz=(-1./2.)*U(i,j,k-1)+(1./2.)*U(i,j,k+1)
            dvdz=(-1./2.)*V(i,j,k-1)+(1./2.)*V(i,j,k+1)
            dwdz=(-1./2.)*W(i,j,k-1)+(1./2.)*W(i,j,k+1)
          else if (k==zpmax) then
            dudz=(3./2.)*U(i,j,k)+(-2.)*U(i,j,k-1)+(1./2.)*U(i,j,k-2)
            dvdz=(3./2.)*V(i,j,k)+(-2.)*V(i,j,k-1)+(1./2.)*V(i,j,k-2)
            dwdz=(3./2.)*W(i,j,k)+(-2.)*W(i,j,k-1)+(1./2.)*W(i,j,k-2)
          else
            dudz=(1./12.)*U(i,j,k-2)+(-2./3.)*U(i,j,k-1)+(2./3.)*U(i,j,k+1)+(-1./12.)*U(i,j,k+2)
            dvdz=(1./12.)*V(i,j,k-2)+(-2./3.)*V(i,j,k-1)+(2./3.)*V(i,j,k+1)+(-1./12.)*V(i,j,k+2)
            dwdz=(1./12.)*W(i,j,k-2)+(-2./3.)*W(i,j,k-1)+(2./3.)*W(i,j,k+1)+(-1./12.)*W(i,j,k+2)
          end if
          dudz=dudz/dz
          dvdz=dvdz/dz
          dwdz=dwdz/dz

        s11(i,j,k)=dudx
        s22(i,j,k)=dvdy
        s33(i,j,k)=dwdz
        s12(i,j,k)=0.5*(dudy+dvdx)
        s13(i,j,k)=0.5*(dudz+dwdx)
        s23(i,j,k)=0.5*(dvdz+dwdy)

        VOR_x(i,j,k)=dwdy-dvdz
        VOR_y(i,j,k)=dudz-dwdx
        VOR_z(i,j,k)=dvdx-dudy

        VOR(i,j,k)=sqrt(VOR_x(i,j,k)**2+VOR_y(i,j,k)**2+VOR_z(i,j,k)**2)
        DIS(i,j,k)=s11(i,j,k)**2+s22(i,j,k)**2+s33(i,j,k)**2+2.*s12(i,j,k)**2+2.*s13(i,j,k)**2+2.*s23(i,j,k)**2

      end do
    end do
  end do
!$OMP END DO
!$OMP END PARALLEL

  

  return
End subroutine

Subroutine RK4(point,dt,point_out,U1,U2,U3,V1,V2,V3,W1,W2,W3,&
                grid_x,grid_y,grid_z,xpmin,xpmax,ypmin,ypmax,zpmin,zpmax,flag)
  implicit none
  
  real::p1(1:3),p2(1:3),p3(1:3),p4(1:3)
  real::h
  integer::box_index1(1:6),box_index2(1:6),box_index3(1:6),box_index4(1:6) !corresponds to x1,x2,y1,y2,z1,z2
  real::k1(1:3),k2(1:3),k3(1:3),k4(1:3)


  integer,intent(in)::xpmin,xpmax,ypmin,ypmax,zpmin,zpmax
  real,intent(in)::dt
  real,dimension(xpmin:xpmax,ypmin:ypmax,zpmin:zpmax),intent(in)::U1,U2,U3
  real,dimension(xpmin:xpmax,ypmin:ypmax,zpmin:zpmax),intent(in)::V1,V2,V3
  real,dimension(xpmin:xpmax,ypmin:ypmax,zpmin:zpmax),intent(in)::W1,W2,W3
  real,intent(in) :: grid_x(xpmin:xpmax), grid_y(ypmin:ypmax), grid_z(zpmin:zpmax)
  real,intent(in)   ::point(1:3)
  real,intent(out)  ::point_out(1:3)
  real::flag

  h=dt
  p1(:)=point(:)

!U1 is U at t, U2 is U at t+dt/2, U3 is U at t+dt

  call find_point(p1,box_index1,grid_x,grid_y,grid_z,xpmin,xpmax,ypmin,ypmax,zpmin,zpmax)

  call trilinear(p1,grid_x,grid_y,grid_z,U1,k1(1),xpmin,xpmax,ypmin,ypmax,zpmin,zpmax)
  call trilinear(p1,grid_x,grid_y,grid_z,V1,k1(2),xpmin,xpmax,ypmin,ypmax,zpmin,zpmax)
  call trilinear(p1,grid_x,grid_y,grid_z,W1,k1(3),xpmin,xpmax,ypmin,ypmax,zpmin,zpmax)

  p2(:)=p1(:)+k1(:)*h/2

  if (p2(1)>grid_x(xpmax) .or. p2(1)<grid_x(xpmin) .or. &
      p2(2)>grid_y(ypmax) .or. p2(2)<grid_y(xpmin) .or. &
      p2(3)>grid_z(zpmax) .or. p2(3)<grid_z(xpmin) ) then
    flag=1
    return 
  end if

  call find_point(p2,box_index2,grid_x,grid_y,grid_z,xpmin,xpmax,ypmin,ypmax,zpmin,zpmax)

  call trilinear(p2,grid_x,grid_y,grid_z,U2,k2(1),xpmin,xpmax,ypmin,ypmax,zpmin,zpmax)
  call trilinear(p2,grid_x,grid_y,grid_z,V2,k2(2),xpmin,xpmax,ypmin,ypmax,zpmin,zpmax)
  call trilinear(p2,grid_x,grid_y,grid_z,W2,k2(3),xpmin,xpmax,ypmin,ypmax,zpmin,zpmax)

  p3=p1+k2*h/2
  
  if (p3(1)>grid_x(xpmax) .or. p3(1)<grid_x(xpmin) .or. &
      p3(2)>grid_y(ypmax) .or. p3(2)<grid_y(xpmin) .or. &
      p3(3)>grid_z(zpmax) .or. p3(3)<grid_z(xpmin) ) then
    flag=1
    return 
  end if

  call find_point(p3,box_index3,grid_x,grid_y,grid_z,xpmin,xpmax,ypmin,ypmax,zpmin,zpmax)

  call trilinear(p3,grid_x,grid_y,grid_z,U2,k3(1),xpmin,xpmax,ypmin,ypmax,zpmin,zpmax)
  call trilinear(p3,grid_x,grid_y,grid_z,V2,k3(2),xpmin,xpmax,ypmin,ypmax,zpmin,zpmax)
  call trilinear(p3,grid_x,grid_y,grid_z,W2,k3(3),xpmin,xpmax,ypmin,ypmax,zpmin,zpmax)

  p4=p1+k3*h

  if (p4(1)>grid_x(xpmax) .or. p4(1)<grid_x(xpmin) .or. &
      p4(2)>grid_y(ypmax) .or. p4(2)<grid_y(xpmin) .or. &
      p4(3)>grid_z(zpmax) .or. p4(3)<grid_z(xpmin) ) then
    flag=1
    return 
  end if

  call find_point(p4,box_index4,grid_x,grid_y,grid_z,xpmin,xpmax,ypmin,ypmax,zpmin,zpmax)

  call trilinear(p4,grid_x,grid_y,grid_z,U3,k4(1),xpmin,xpmax,ypmin,ypmax,zpmin,zpmax)
  call trilinear(p4,grid_x,grid_y,grid_z,V3,k4(2),xpmin,xpmax,ypmin,ypmax,zpmin,zpmax)
  call trilinear(p4,grid_x,grid_y,grid_z,W3,k4(3),xpmin,xpmax,ypmin,ypmax,zpmin,zpmax)

  point_out=point+k1*h/6+k2*h/3+k3*h/3+k4*h/6

  !print*, point(1:3), point_out(1:3)

  !print*,dt

  return 
End subroutine


Subroutine RK4_inertia(point,up,point_out,upout,dt,taup,U1,U2,U3,V1,V2,V3,W1,W2,W3,&
                            grid_x,grid_y,grid_z,xpmin,xpmax,ypmin,ypmax,zpmin,zpmax)
  implicit none
  
  real,dimension(3),intent(in)::point
  real,intent(in)::dt
  real,dimension(3)::x1,x2,x3,x4
  real::h
  real,dimension(3)::up,upout
  integer,dimension(6)::box_index1,box_index2,box_index3,box_index4 !corresponds to x1,x2,y1,y2,z1,z2
  real,dimension(3)::k1,k2,k3,k4,kp1,kp2,kp3,kp4
  real,dimension(3)::uf1,uf2,uf3,uf4,up1,up2,up3,up4
  real::taup

  integer,intent(in)::xpmin,xpmax,ypmin,ypmax,zpmin,zpmax
  real,dimension(xpmin:xpmax,ypmin:ypmax,zpmin:zpmax),intent(in)::U1,U2,U3
  real,dimension(xpmin:xpmax,ypmin:ypmax,zpmin:zpmax),intent(in)::V1,V2,V3
  real,dimension(xpmin:xpmax,ypmin:ypmax,zpmin:zpmax),intent(in)::W1,W2,W3
  real,intent(in) :: grid_x(xpmin:xpmax), grid_y(ypmin:ypmax), grid_z(zpmin:zpmax)
  real,dimension(3),intent(out)::point_out


  h=dt
  x1=point
  up1=up

!U1 is U at t, U2 is U at t+dt/2, U3 is U at t+dt

  call find_point(x1,box_index1,grid_x,grid_y,grid_z,xpmin,xpmax,ypmin,ypmax,zpmin,zpmax)

!Find velocity at p1
  call trilinear(x1,grid_x,grid_y,grid_z,U1,uf1(1),xpmin,xpmax,ypmin,ypmax,zpmin,zpmax)
  call trilinear(x1,grid_x,grid_y,grid_z,V1,uf1(2),xpmin,xpmax,ypmin,ypmax,zpmin,zpmax)
  call trilinear(x1,grid_x,grid_y,grid_z,W1,uf1(3),xpmin,xpmax,ypmin,ypmax,zpmin,zpmax)
  up1=up

  k1=(uf1-up1)/taup

  kp1=up1

  x2=x1+kp1*h/2.d0

  call find_point(x2,box_index2,grid_x,grid_y,grid_z,xpmin,xpmax,ypmin,ypmax,zpmin,zpmax)

  call trilinear(x2,grid_x,grid_y,grid_z,U2,uf2(1),xpmin,xpmax,ypmin,ypmax,zpmin,zpmax)
  call trilinear(x2,grid_x,grid_y,grid_z,V2,uf2(2),xpmin,xpmax,ypmin,ypmax,zpmin,zpmax)
  call trilinear(x2,grid_x,grid_y,grid_z,W3,uf2(3),xpmin,xpmax,ypmin,ypmax,zpmin,zpmax)

  up2=up1+k1*h/2.d0

  k2=(uf2-up2)/taup

  kp2=up2

  x3=x1+kp2*h/2.d0

  call find_point(x3,box_index3,grid_x,grid_y,grid_z,xpmin,xpmax,ypmin,ypmax,zpmin,zpmax)

  call trilinear(x3,grid_x,grid_y,grid_z,U2,uf3(1),xpmin,xpmax,ypmin,ypmax,zpmin,zpmax)
  call trilinear(x3,grid_x,grid_y,grid_z,V2,uf3(2),xpmin,xpmax,ypmin,ypmax,zpmin,zpmax)
  call trilinear(x3,grid_x,grid_y,grid_z,V3,uf3(3),xpmin,xpmax,ypmin,ypmax,zpmin,zpmax)

  up3=up1+k2*h/2.d0

  k3=(uf3-up3)/taup

  kp3=up3

  x4=x1+kp3*h

  call find_point(x4,box_index4,grid_x,grid_y,grid_z,xpmin,xpmax,ypmin,ypmax,zpmin,zpmax)

  call trilinear(x4,grid_x,grid_y,grid_z,U3,uf4(1),xpmin,xpmax,ypmin,ypmax,zpmin,zpmax)
  call trilinear(x4,grid_x,grid_y,grid_z,V3,uf4(2),xpmin,xpmax,ypmin,ypmax,zpmin,zpmax)
  call trilinear(x4,grid_x,grid_y,grid_z,W3,uf4(3),xpmin,xpmax,ypmin,ypmax,zpmin,zpmax)

  up4=up1+k3*h

  k4=(uf4-up4)/taup

  kp4=up4

  upout=up+(h/6.d0)*(k1+2.d0*k2+2.d0*k3+k4)
  point_out=point+(h/6.d0)*(kp1+2.d0*kp2+2.d0*kp3+kp4)

  return
End subroutine


Subroutine RK4_inertia_g(point,up,point_out,upout,dt,taup,U1,U2,U3,V1,V2,V3,W1,W2,W3,&
                            grid_x,grid_y,grid_z,xpmin,xpmax,ypmin,ypmax,zpmin,zpmax,dir,g)
  implicit none
  
  real,dimension(3),intent(in)::point
  real,intent(in)::dt
  real,dimension(3)::x1,x2,x3,x4
  real::h
  real,dimension(3)::up,upout
  integer,dimension(6)::box_index1,box_index2,box_index3,box_index4 !corresponds to x1,x2,y1,y2,z1,z2
  real,dimension(3)::k1,k2,k3,k4,kp1,kp2,kp3,kp4
  real,dimension(3)::uf1,uf2,uf3,uf4,up1,up2,up3,up4
  real::taup

  integer,intent(in)::xpmin,xpmax,ypmin,ypmax,zpmin,zpmax
  real,dimension(xpmin:xpmax,ypmin:ypmax,zpmin:zpmax),intent(in)::U1,U2,U3
  real,dimension(xpmin:xpmax,ypmin:ypmax,zpmin:zpmax),intent(in)::V1,V2,V3
  real,dimension(xpmin:xpmax,ypmin:ypmax,zpmin:zpmax),intent(in)::W1,W2,W3
  real,intent(in) :: grid_x(xpmin:xpmax), grid_y(ypmin:ypmax), grid_z(zpmin:zpmax)
  real,dimension(3),intent(out)::point_out

  integer,intent(in)::dir
  real,intent(in)::g

  h=dt
  x1=point
  up1=up


  if (dir/=1 .and. dir/=2 .and. dir/=3) then
    print*, 'gravity direction is wrong!'
    stop
  end if

!U1 is U at t, U2 is U at t+dt/2, U3 is U at t+dt

  call find_point(x1,box_index1,grid_x,grid_y,grid_z,xpmin,xpmax,ypmin,ypmax,zpmin,zpmax)

!Find velocity at p1
  call trilinear(x1,grid_x,grid_y,grid_z,U1,uf1(1),xpmin,xpmax,ypmin,ypmax,zpmin,zpmax)
  call trilinear(x1,grid_x,grid_y,grid_z,V1,uf1(2),xpmin,xpmax,ypmin,ypmax,zpmin,zpmax)
  call trilinear(x1,grid_x,grid_y,grid_z,W1,uf1(3),xpmin,xpmax,ypmin,ypmax,zpmin,zpmax)
  up1=up

  k1=(uf1-up1)/taup
  k1(dir)=k1(dir)+g

  kp1=up1

  x2=x1+kp1*h/2.d0

  call find_point(x2,box_index2,grid_x,grid_y,grid_z,xpmin,xpmax,ypmin,ypmax,zpmin,zpmax)

  call trilinear(x2,grid_x,grid_y,grid_z,U2,uf2(1),xpmin,xpmax,ypmin,ypmax,zpmin,zpmax)
  call trilinear(x2,grid_x,grid_y,grid_z,V2,uf2(2),xpmin,xpmax,ypmin,ypmax,zpmin,zpmax)
  call trilinear(x2,grid_x,grid_y,grid_z,W3,uf2(3),xpmin,xpmax,ypmin,ypmax,zpmin,zpmax)

  up2=up1+k1*h/2.d0

  k2=(uf2-up2)/taup
  k2(dir)=k2(dir)+g

  kp2=up2

  x3=x1+kp2*h/2.d0

  call find_point(x3,box_index3,grid_x,grid_y,grid_z,xpmin,xpmax,ypmin,ypmax,zpmin,zpmax)

  call trilinear(x3,grid_x,grid_y,grid_z,U2,uf3(1),xpmin,xpmax,ypmin,ypmax,zpmin,zpmax)
  call trilinear(x3,grid_x,grid_y,grid_z,V2,uf3(2),xpmin,xpmax,ypmin,ypmax,zpmin,zpmax)
  call trilinear(x3,grid_x,grid_y,grid_z,V3,uf3(3),xpmin,xpmax,ypmin,ypmax,zpmin,zpmax)

  up3=up1+k2*h/2.d0

  k3=(uf3-up3)/taup
  k3(dir)=k3(dir)+g

  kp3=up3

  x4=x1+kp3*h

  call find_point(x4,box_index4,grid_x,grid_y,grid_z,xpmin,xpmax,ypmin,ypmax,zpmin,zpmax)

  call trilinear(x4,grid_x,grid_y,grid_z,U3,uf4(1),xpmin,xpmax,ypmin,ypmax,zpmin,zpmax)
  call trilinear(x4,grid_x,grid_y,grid_z,V3,uf4(2),xpmin,xpmax,ypmin,ypmax,zpmin,zpmax)
  call trilinear(x4,grid_x,grid_y,grid_z,W3,uf4(3),xpmin,xpmax,ypmin,ypmax,zpmin,zpmax)

  up4=up1+k3*h

  k4=(uf4-up4)/taup
  k4(dir)=k4(dir)+g

  kp4=up4

  upout=up+(h/6.d0)*(k1+2.d0*k2+2.d0*k3+k4)
  point_out=point+(h/6.d0)*(kp1+2.d0*kp2+2.d0*kp3+kp4)

  return
End subroutine


Subroutine z_direction_correc(point,grid_z,zpmin,zpmax)
  implicit none
  
  integer,intent(in)::zpmin,zpmax
  real,dimension(3)::point
  real,intent(in) :: grid_z(zpmin:zpmax)

  if (point(3)>grid_z(zpmax)) then
    point(3)=point(3)-grid_z(zpmax)
  else if (point(3)<grid_z(zpmin)) then
    point(3)=grid_z(zpmax)-(grid_z(zpmin)-point(3))
  end if

end subroutine
