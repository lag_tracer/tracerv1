module droplet

  implicit none

  !Unit has to be in [kg m s mol]

  real(4), parameter:: R = 8.314462 !Gas constant [J K-1 mol-1 = kg m2 s-2 k-1 mol-1]
  real(4), parameter:: Mw = 0.01801528 ! Water Molecule Weight [kg mol-1]
  real(4), parameter:: e = 2.7182818 ! natural logarithm base


  real(4):: Tinf ! Reference Temperature [K]

  real(4):: Dv !Diffusion Coefficient of Water Vapor [m2 s-1]
  real(4):: rho_w !Water vapor density [kg m-3]
  real(4):: e_sat_w !Saturation pressure of water [Pa = kg m-1 s-2]
  real(4):: Le0 !Specific Latent Heat of evaporation []
  real(4):: gamma_le0 !exponent on specific latent heat of evaporation
  real(4):: Ka  !Thremal Conductivity of Dry Air [?]

  !real(4):: A3  !Droplet Growth Constant [m2 s-1]
  real(4):: A3a !saturation pressure part
  real(4):: A3b !latent heat part

  contains

  subroutine compute_A3(A)

  implicit none
  real(4)::A
  real(4)::A3

  e_sat_w=e**(77.3450 + 0.0057*Tinf -7235/Tinf)/Tinf**8.2
  rho_w=1000
  !Dv=0.211*(101325.d0/e_sat_w)*((Tinf/273.15)**1.94)*(10.0**(-4))
  Dv=10.d0**(-5)


  print*, 'e_sat_w[Pa] rho_w[kg m-3]',e_sat_w, rho_w
  print*, 'Dv[m2 s-1]',Dv

  gamma_le0 = 0.167+3.67*10**(-4)*Tinf
  Le0 = 597.3*(273.15/Tinf)**gamma_le0

  Ka=(5.69+0.017*Tinf)*10**(-5)

  !
  A3a=rho_w*R*Tinf/(e_sat_w*Dv*Mw)
  !Specific Latent Heat Part (how?)
  A3b=Le0*rho_w/(Ka*Tinf)*(Le0*Mw/(Tinf*R)-1)

  !A3=A3a+A3b
  A3=1.0d0/A3a

  A=A3

  print*, 'A3 A3a A3b',A3, A3a, A3b

  end subroutine compute_A3

  subroutine droplet_growth(A,supersaturation,radius,radius_new,dt,flag)  

  real(4)::A,supersaturation,radius,radius_new,dt
  logical::flag

  radius_new=radius+A*supersaturation*dt/radius

  if (radius_new>radius*2.0 .or. radius_new<radius*0.5 .or. radius_new<0. ) then
    !print*, radius, radius_new, 'radius seems wrong'
    flag=.true.
  else
    flag=.false.
  end if

  !print*, radius, radius_new, supersaturation, dt

  end subroutine

  subroutine compute_taup(rho_a,nu,radius,taup)

  implicit none

  real(4)::rho_a,nu,taup,radius

  taup=rho_w*radius**2/(18*rho_a*nu)

  end subroutine
 
end module droplet
