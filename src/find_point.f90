Subroutine trilinear(point,grid_x,grid_y,grid_z,var,c,xpmin,xpmax,ypmin,ypmax,zpmin,zpmax)
  Implicit none  
  Integer i,j,k
!FIX IT!
  integer,intent(in)::xpmin,xpmax,ypmin,ypmax,zpmin,zpmax  
  real,intent(in)   :: grid_x(xpmin:xpmax), grid_y(ypmin:ypmax), grid_z(zpmin:zpmax)
  real,intent(in)   :: var(xpmin:xpmax,ypmin:ypmax,zpmin:zpmax)
  real,intent(in)   :: point(1:3)
  integer:: box_index(1:6)
  real,intent(out)::c
  real::x1,x2,y1,y2,z1,z2
  real::xd,yd,zd
  real::c00,c01,c10,c11
  real::c0,c1


  call find_point(point,box_index,grid_x,grid_y,grid_z,xpmin,xpmax,ypmin,ypmax,zpmin,zpmax)

  x1=grid_x(box_index(1))
  x2=grid_x(box_index(2))
  y1=grid_y(box_index(3))
  y2=grid_y(box_index(4))
  z1=grid_z(box_index(5))
  z2=grid_z(box_index(6))

  if (box_index(1)==box_index(2)) then
    xd=1
  else
    xd=(point(1)-x1)/(x2-x1)
  end if

  if (box_index(3)==box_index(4)) then
    yd=1
  else
    yd=(point(2)-y1)/(y2-y1)
  end if

  if (box_index(5)==box_index(6)) then
    zd=1
  else
    zd=(point(3)-z1)/(z2-z1)
  end if

  !To ckeck the values
  !if (box_index(5)==box_index(6)) then
  !  write(*,*) 'xd, yd, zd', xd, yd, zd
  !end if

  c00=var(box_index(1),box_index(3),box_index(5))*(1-xd)+var(box_index(2),box_index(3),box_index(5))*xd
  c01=var(box_index(1),box_index(3),box_index(6))*(1-xd)+var(box_index(2),box_index(3),box_index(6))*xd
  c10=var(box_index(1),box_index(4),box_index(5))*(1-xd)+var(box_index(2),box_index(4),box_index(5))*xd
  c11=var(box_index(1),box_index(4),box_index(6))*(1-xd)+var(box_index(2),box_index(4),box_index(6))*xd
  
  c0=c00*(1-yd)+c10*yd
  c1=c01*(1-yd)+c11*yd

  c=c0*(1-zd)+c1*zd

  !To check the values
  !write(*,*) var(box_index(1),box_index(3),box_index(5)),var(box_index(2),box_index(3),box_index(5))
  !write(*,*) var(box_index(1),box_index(3),box_index(6)),var(box_index(2),box_index(3),box_index(6))
  !write(*,*) var(box_index(1),box_index(4),box_index(5)),var(box_index(2),box_index(4),box_index(5))
  !write(*,*) var(box_index(1),box_index(4),box_index(6)),var(box_index(2),box_index(4),box_index(6))
  !write(*,*) 'c', c


  !To check center of var
  !write(*,*) 'var center', (var(box_index(1),box_index(3),box_index(5))+var(box_index(2),box_index(3),box_index(5))+&
  !var(box_index(1),box_index(3),box_index(6))+var(box_index(2),box_index(3),box_index(6))+&
  !var(box_index(1),box_index(4),box_index(5))+var(box_index(2),box_index(4),box_index(5))+&
  !var(box_index(1),box_index(4),box_index(6))+var(box_index(2),box_index(4),box_index(6)))/8

  return
End Subroutine

Subroutine find_point(point,box_index,grid_x,grid_y,grid_z,xpmin,xpmax,ypmin,ypmax,zpmin,zpmax)
  implicit none
  real,intent(in)::point(0:2)
  real::x1,x2,y1,y2,z1,z2
  integer::i,j,k

  integer,intent(in)::xpmin,xpmax,ypmin,ypmax,zpmin,zpmax  
  real,intent(in) :: grid_x(xpmin:xpmax), grid_y(ypmin:ypmax), grid_z(zpmin:zpmax)
  integer,intent(out)::box_index(0:5) !corresponds to x1,x2,y1,y2,z1,z2

  !To check the values
  !write(*,*) 'xp,yp,zp',point

  i=xpmin+minloc(abs(grid_x-point(0)),dim=1)-1
  if (grid_x(i)<point(0)) then
    box_index(0)=i
    box_index(1)=i+1
  else if (grid_x(i)==point(0)) then
    box_index(0)=i
    box_index(1)=i
  else
    box_index(0)=i-1
    box_index(1)=i
  end if

  j=ypmin+minloc(abs(grid_y-point(1)),dim=1)-1
  if (grid_y(j)<point(1)) then
    box_index(2)=j
    box_index(3)=j+1
  else if (grid_y(j)==point(1)) then
    box_index(2)=j
    box_index(3)=j
  else
    box_index(2)=j-1
    box_index(3)=j
  end if

  k=zpmin+minloc(abs(grid_z-point(2)),dim=1)-1
  if (grid_z(k)<point(2)) then
    box_index(4)=k
    box_index(5)=k+1
  else if (grid_z(k)==point(2)) then
    box_index(4)=k
    box_index(5)=k
  else
    box_index(4)=k-1
    box_index(5)=k
  end if

  if (i>xpmax) then
    i=xpmax
  end if 
  if (j>ypmax) then
    j=ypmax
  end if
  if (k>zpmax) then
    k=zpmax
  end if 

!To check the values
!  write(*,*) 'x',i,grid_x(box_index(0)),point(0),grid_x(box_index(1))
!  write(*,*) 'y',j,grid_y(box_index(2)),point(1),grid_y(box_index(3))
!  write(*,*) 'z',k,grid_z(box_index(4)),point(2),grid_z(box_index(5))
!  write(*,*) 'box_index',box_index
  
  return
End subroutine
