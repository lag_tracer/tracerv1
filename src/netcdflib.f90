SUBROUTINE create_netcdf(file_name,n)

  use netcdf
  implicit none
  include 'netcdf.inc'
    
  integer,parameter :: ndim=2
  integer :: dimid(ndim)
  integer :: ncid
  integer,intent(in) :: n

  character(len=*),intent(in) :: file_name

  logical :: file_exist
  integer:: wanna_use_current_file

  inquire(file=file_name,exist=file_exist)
  
!write(*,*)'inquire',file_exist

  if (file_exist) then
     write(*,*)'NetCDF file ', file_name, ' already exists.'
     write(*,*)'Would you like to use current file?'
     write(*,*)'1-Use current NetCDF file'
     write(*,*)'2-Delete current NetCDF file'
     write(*,*)'3-Stop execution'

     do while (wanna_use_current_file /= 1 .or. &
               wanna_use_current_file /= 2 .or. &
               wanna_use_current_file /= 3 )

       read(*,*)wanna_use_current_file
       !wanna_use_current_file=2

       if (wanna_use_current_file == 1) then
         call check(nf_open(file_name,nf_write,ncid))
       else if (wanna_use_current_file == 2) then
         open(unit=100, file=file_name, status='old')
         close(100,status='delete')

         write(*,*)'Now Creating New NetCDF file'
         call check(nf90_create(file_name,nf90_64bit_offset,ncid))
         call check(nf90_def_dim(ncid,'n',n,dimid(1)))
         call check(nf90_def_dim(ncid,'time',nf90_unlimited,dimid(2)))
         call check(nf_enddef(ncid))
         !End of definition
         call check(nf_close(ncid))
         exit
        
         write(*,*)'success creating netcdf file'

       else if (wanna_use_current_file == 3) then
         stop
       else
         write(*,*)'Please enter valid value. 1, 2 or 3.'
         write(*,*)'1-Use current NetCDF file'
         write(*,*)'2-Delete current NetCDF file'
         write(*,*)'3-Stop execution'
       end if
     end do

  else

    write(*,*)'No NetCDF File found. Creating NetCDF file'
    !Create NetCDF
    call check(nf90_create(file_name,nf90_64bit_offset,ncid))
    call check(nf90_def_dim(ncid,'n',n,dimid(1)))
    call check(nf90_def_dim(ncid,'time',nf90_unlimited,dimid(2)))
    call check(nf_enddef(ncid))
    !End of definition
    call check(nf_close(ncid))
    
    write(*,*)'success creating netcdf file'

  end if



  return
end subroutine

SUBROUTINE write_netcdf(file_name,var_name,var,n,t)

  use netcdf
  implicit none
  include 'netcdf.inc'
    
  real(4),intent(in) :: var(n)

  integer,parameter :: ndim=2
  integer :: dimlen(ndim),istart(ndim),icount(ndim),imap(ndim),dimid(ndim)
  integer :: ncid,i,varid
  integer,intent(in) :: n,t
  integer :: status
  integer :: dim_len_check
  character(len=14) :: dim_name(ndim)

  character(len=*),intent(in) :: file_name,var_name

  logical :: file_exist

  dim_name(1) = 'n'
  dim_name(2) = 'time'
  
  inquire(file=file_name,exist=file_exist)
  
!write(*,*)'inquire',file_exist

  if (file_exist) then
     call check(nf_open(file_name,nf_write,ncid))

  else
    write(*,*) 'Can not find netcdf file. Stop execution.'
    stop

  end if
  
  do i=1,ndim
    call check(nf90_inq_dimid(ncid,dim_name(i),dimid(i)))
    call check(nf90_inquire_dimension(ncid,dimid(i),len=dimlen(i)))
  end do


  call check(nf90_redef(ncid))
  status = nf90_inq_varid(ncid,var_name,varid)
  if (status.ne.nf_noerr) then
    call check(nf90_def_var(ncid,var_name,nf90_float,dimid,varid))
  end if
  call check(nf_enddef(ncid))
  !End of definition

  istart(1) = 1
  istart(2) = t+1

  icount(1) = n
  icount(2) = 1             

  !write(*,*) 'now writing ---> ',var_name,'....................'
  call check(nf90_put_var(ncid,varid,var,istart,icount))
  call check(nf_close(ncid))

  
  
  !write(*,*)'success writing ---> ',var_name 

  return
end subroutine write_netcdf


SUBROUTINE read_grid_netcdf(filename,var_name,var,nx)

    use netcdf
    implicit none
    include 'netcdf.inc'

    integer :: idvar
    integer :: ncid

    character(len=*),intent(in)::filename,var_name
    integer,intent(in)::nx
    real,intent(out)::var(nx)


!=============== Open files ================

    call check(nf90_open(filename,nf90_nowrite,ncid))
    !print *,'Reading ...'

!=========== Read field ====================

!The function NF_INQ_VARID returns the ID of a netCDF variable, given its name
    call check(nf90_inq_varid(ncid,var_name,idvar))
    call check(nf90_get_var(ncid,idvar,var))

    call check(nf90_close(ncid))

    !print *,'Finish Reading Value ', var_name    

  return
end subroutine read_grid_netcdf

SUBROUTINE read_netcdf(filename,var_name,var,xpmin,xpmax,ypmin,ypmax,zpmin,zpmax)

    use netcdf
    implicit none
    include 'netcdf.inc'

    integer :: idvar
    integer :: iddim(3)
    integer :: lenghtx,lenghty,lenghtz
    integer :: ncid
    integer :: start(3),count(3)

    character(len=*),intent(in)::filename,var_name
    integer,intent(in)::xpmin,xpmax,ypmin,ypmax,zpmin,zpmax
    real,intent(out)::var(xpmin:xpmax,ypmin:ypmax,zpmin:zpmax)


!=============== Open files ================

    call check(nf90_open(filename,nf90_nowrite,ncid))
    !print *,'Reading ...'

!=========== Read field ====================

    start(1) = xpmin+1
    start(2) = ypmin+1
    start(3) = zpmin+1

    count(1) = xpmax-xpmin+1
    count(2) = ypmax-ypmin+1
    count(3) = zpmax-zpmin+1


!The function NF_INQ_VARID returns the ID of a netCDF variable, given its name
    call check(nf90_inq_varid(ncid,var_name,idvar))
    call check(nf90_get_var(ncid,idvar,var,start=start,count=count))

    call check(nf90_close(ncid))

    print *,'Finish Reading Value ', var_name    

  return
end subroutine read_netcdf 

subroutine check(status)
    use netcdf
    integer, intent (in) :: status
    
    if(status /= nf90_noerr) then 
      write(*,*) nf90_strerror(status)
      stop "Stopped"
    end if
end subroutine check 

subroutine check_through(status)
    use netcdf
    integer, intent (inout) :: status
    
    if(status /= nf90_noerr) then 
      write(*,*) nf90_strerror(status)
      write(*,*) "Netcdf has an error but continue......"
    end if
end subroutine check_through

SUBROUTINE read_ens_general(filename,var1,nx,ny,nz,mytype)

    !$ use omp_lib
    implicit none
    character(len=*),intent(in)::filename
    real*4,dimension(nx,ny,nz),intent(out)::var1
    integer::i,j,k
    integer,intent(in)::nx,ny,nz
    integer::mytype
    integer::nb_procs
    real*4::t1,t2

!$OMP PARALLEL
!$  nb_procs = OMP_GET_NUM_THREADS()
!$OMP END PARALLEL
    
!=========== Read field ====================

!The function NF_INQ_VARID returns the ID of a netCDF variable, given its name
print*, 'Reading ',trim(filename)

open(unit=1000,file=filename,status='old',form='unformatted',access='direct',recl=mytype*nx*ny)
call cpu_time(t1)

do k=1,nz
  read(1000,rec=k) var1(:,:,k)
end do

call cpu_time(t2)

close(1000)

print*,'file reading time' ,t2-t1

return

end subroutine

SUBROUTINE read_point(filename,var1,xpmin,xpmax,ypmin,ypmax,zp,time)

    !$ use omp_lib
    implicit none
    include 'netcdf.inc'

    character(len=150),intent(in)::filename
    !character(len=1),intent(in)::var_name
    real*4,dimension(xpmin:xpmax,ypmin:ypmax),intent(out)::var1
    real*4,allocatable,dimension(:,:,:,:)::var_temp
    integer::i,j,k,l
    integer*8::count,count_local
    integer*8::xsize,ysize,zsize
    integer*4,intent(in)::time
    !integer,intent(in)::xpmin,xpmax,ypmin,ypmax,zpmin,zpmax
    integer::xpmin,xpmax,ypmin,ypmax,zpmin,zpmax
    integer::zp
    integer::mytype
    real*4::temp
    integer::vartype
    integer::nb_procs
    real*4::t1,t2,t3,t4

!$OMP PARALLEL
!$  nb_procs = OMP_GET_NUM_THREADS()
!$OMP END PARALLEL

!=============== Open files ================
mytype=4
    
!=========== Read field ====================

!The function NF_INQ_VARID returns the ID of a netCDF variable, given its name
count=1
xsize=751
ysize=1361
zsize=300


!if (var_name=='U') then
!  vartype=1
!else if (var_name=='V') then
!  vartype=2
!else if (var_name=='W') then
!  vartype=3
!else if (var_name=='P') then
!  vartype=4
!end if

count=count+zsize*int(time,8)
print*,'rec',count,'vartype',vartype,'domain',zp

allocate(var_temp(0:xsize-1,1:4,0:ysize-1,1))

!WE CANNOT READ var2(:,:,:,:) because of the compiler!
open(unit=0,file=filename,status='old',form='unformatted',access='direct',recl=mytype*xsize*ysize*4)
call cpu_time(t3)

k=zp

read(0,rec=k+count) var_temp(:,:,:,1)

var1(:,:)=var_temp(:,1,:,1)

call cpu_time(t4)

close(0)

print*,'file reading time' ,t4-t3

return

end subroutine read_point

SUBROUTINE read_binary(filename,var1,var2,var3,xpmin,xpmax,ypmin,ypmax,zpmin,zpmax,time)

    !$ use omp_lib
    implicit none
    include 'netcdf.inc'

    character(len=150),intent(in)::filename
    !character(len=1),intent(in)::var_name
    real*4,dimension(xpmin:xpmax,ypmin:ypmax,zpmin:zpmax),intent(out)::var1,var2,var3
    real*4,allocatable,dimension(:,:,:,:)::var_temp
    integer::i,j,k,l
    integer*8::count,count_local
    integer*8::xsize,ysize,zsize
    integer*4,intent(in)::time
    !integer,intent(in)::xpmin,xpmax,ypmin,ypmax,zpmin,zpmax
    integer::xpmin,xpmax,ypmin,ypmax,zpmin,zpmax
    integer::mytype
    real*4::temp
    integer::vartype
    integer::nb_procs
    real*4::t1,t2,t3,t4

!$OMP PARALLEL
!$  nb_procs = OMP_GET_NUM_THREADS()
!$OMP END PARALLEL

!=============== Open files ================
mytype=4
    
!=========== Read field ====================

!The function NF_INQ_VARID returns the ID of a netCDF variable, given its name
count=1
xsize=751
ysize=1361
zsize=300


!if (var_name=='U') then
!  vartype=1
!else if (var_name=='V') then
!  vartype=2
!else if (var_name=='W') then
!  vartype=3
!else if (var_name=='P') then
!  vartype=4
!end if

count=count+zsize*int(time,8)
print*,'rec',count,'vartype',vartype,'domain',xpmin,xpmax,ypmin,ypmax,zpmin,zpmax

allocate(var_temp(0:xsize-1,1:4,0:ysize-1,0:zsize-1))

!WE CANNOT READ var2(:,:,:,:) because of the compiler!
open(unit=0,file=filename,status='old',form='unformatted',access='direct',recl=mytype*xsize*ysize*4)
call cpu_time(t3)
do k=1,zsize
  read(0,rec=k+count-1) var_temp(:,:,:,k-1)
end do
var1(:,:,:)=var_temp(xpmin:xpmax,1,ypmin:ypmax,zpmin:zpmax)
var2(:,:,:)=var_temp(xpmin:xpmax,2,ypmin:ypmax,zpmin:zpmax)
var3(:,:,:)=var_temp(xpmin:xpmax,3,ypmin:ypmax,zpmin:zpmax)
call cpu_time(t4)
close(0)


!open(unit=0,file=filename,status='old',form='unformatted',access='direct',recl=mytype)
!
!call cpu_time(t1)
!    do k=1,zsize
!    do j=1,ysize
!    do l=1,4
!    do i=1,xsize
!      !count_local=count+&
!      !            int(i-1,8)+&
!      !            int(l-1,8)*xsize+&
!      !            int(j-1,8)*xsize*int(4,8)+&
!      !            int(k-1,8)*xsize*int(4,8)*ysize
!      if ((l==vartype) .and.&
!           (xpmin<=(i-1) .and. xpmax>=(i-1)) .and.&
!           (ypmin<=(j-1) .and. ypmax>=(j-1)) .and.&
!           (zpmin<=(k-1) .and. zpmax>=(k-1)) ) then
!      read(0,rec=count) var(i-1,j-1,k-1)
!      end if
!
!      count=count+int(1,8)
!    end do;end do;end do;end do
!call cpu_time(t2)
!close(0)


!do k=0,2
!do j=1359,1361
!do i=0,43
!print*,i,j,k, var(i,j,k),var2(i,1,j,k),var3(i,1,j,k),var(i,j,k)-var2(i,1,j,k)
!end do;end do;end do

open(unit=1)
do j=ypmin,ypmax
do i=xpmin,xpmax
write(1,*) var1(i,j,100),var2(i,j,100),var3(i,j,100)
end do;end do
close(1)
!print*,'t2-t1 and t4-t3' ,t2-t1, t4-t3

print*,'file reading time' ,t4-t3
  return
end subroutine read_binary

