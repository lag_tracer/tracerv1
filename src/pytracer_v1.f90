subroutine pytrace_v1(cpus,n,nx,ny,nz, &
                       nu, &
                       taup,g,Rinit,&
                       ssflag, v0flag, ssnorm, &
                       grid_x, grid_y, grid_z, &
                       point_input, &
                       start_time,process_files,t_interval,dt,&
                       fileflag,&
                       input_folder,input_filehead,input_ensnorm,output_filename)

!**********************************************************************
!subroutine pytrace
!subroutine to trace particles callable from python
!Input input file as an first argument + DNS results
!Output NetCDF particle track file
!**********************************************************************

  !$ use omp_lib
  use,intrinsic::ieee_arithmetic  
  use droplet
  implicit none

  integer::nb_procs,cpus
  integer::i,j,k
  integer::l,m
  integer::t
  integer::lcount
  integer::ssflag,v0flag
  real*4, intent(in)::ssnorm
  integer,intent(in)::nx,ny,nz

  character(len=250)::input1U_filename,input1W_filename
  character(len=250)::input1V_filename,input1E_filename
  character(len=250)::input1T_filename
  character(len=250)::input2U_filename,input2W_filename
  character(len=250)::input2V_filename,input2E_filename
  character(len=250)::input2T_filename
  character(len=250)::temp_filename
  character(len=*),intent(in)::output_filename
  character(len=7)  ::current_time1,current_time2
  character(len=*),intent(in) ::input_folder,input_filehead,input_ensnorm

  integer,intent(in)::n
  integer,intent(in)::start_time,process_files,t_interval
  integer::filenumber,fileloc,fileloc2

  integer,intent(in)::fileflag

  real*4, intent(in)::dt
  real*4,intent(in)::nu
  real*4,dimension(0:n-1,0:2),intent(in)::point_input

  real*4,allocatable,dimension(:,:)::point,point_new
  real*4,allocatable,dimension(:,:)::max_ens
  
  !For reading UVWP
  real*4, allocatable, dimension(:,:,:)::UVWP1
  real*4, allocatable, dimension(:,:,:)::UVWP2
  
  real*4, allocatable, dimension(:,:,:)::U1,V1,W1
  real*4, allocatable, dimension(:)::Ens_Norm
  real*4, allocatable, dimension(:,:,:)::U2,V2,W2
  real*4, allocatable, dimension(:,:,:)::U3,V3,W3
  
  real*4, allocatable, dimension(:,:,:)::VOR,DIS,SS

  real*4:: grid_x(1:nx), grid_y(1:ny), grid_z(1:nz)  
  real*4:: dx, dy, dz 

  real*4,intent(in)::taup,g,Rinit
  real*4::A3
  real*4::mean_R
  real*4::mean_D
  integer::count,countRK

  logical::flag

  real*4::xmin,xmax,ymin,ymax,zmin,zmax
  integer::xstart,ystart,xpmin,xpmax,ypmin,ypmax,zpmin,zpmax

Tinf=283
call compute_A3(A3)

!$OMP PARALLEL
!$  nb_procs = OMP_GET_NUM_THREADS()
!$OMP END PARALLEL
      write(*,*)'---------------------------'
      write(*,*)'Number of processors :',nb_procs
      write(*,*)'---------------------------'

    write(*,*)'***********************************************'
    write(*,*)'Timestep= ',0
    write(*,*)'Current_time= ',start_time 
    write(*,*)'***********************************************'

print*, 'Input information from Python'
    write(*,*)'***********************************************'
print*, 'n nx ny nz', n, nx, ny, nz
print*, 'taup g', taup,g
print*, 'point input x min max', minval(point_input(:,0)), maxval(point_input(:,0))
print*, 'point input y min max', minval(point_input(:,1)), maxval(point_input(:,1))
print*, 'point input z min max', minval(point_input(:,2)), maxval(point_input(:,2))
print*, 'start_time', start_time
print*, 'process_files',process_files
print*, 't_interval',t_interval
print*, 'dt',dt
print*, 'input_folder ',input_folder
print*, 'input_filehead ',input_filehead
print*, 'input_ensnorm ',input_ensnorm
print*, 'output_filename ',output_filename

write(*,*)'***********************************************'

dx=grid_x(2)-grid_x(1)
dy=grid_y(2)-grid_y(1)
dz=grid_z(2)-grid_z(1)

print*, 'dx dy dz = ', dx,dy,dz

allocate(point(0:n-1,0:10),point_new(0:n-1,0:10))
allocate(UVWP1(nx*8,ny,nz))
allocate(UVWP2(nx*8,ny,nz))

point(:,7)=0.0
point(:,8)=Rinit !Initial Particle Radius
!point(:,9) !Supersaturation
point(:,10)=taup !particle response time

!Copy input points
point(:,0)=point_input(:,0)
point(:,1)=point_input(:,1)
point(:,2)=point_input(:,2)

xmin=minval(point(:,0))
xmax=maxval(point(:,0))
ymin=minval(point(:,1))
ymax=maxval(point(:,1)) 
zmin=minval(point(:,2))
zmax=maxval(point(:,2))

print*, 'dt=',dt

!Read Entire Grid
write(current_time1,"(I07.7)") start_time

if (fileflag==0) then
  input1U_filename=trim(input_folder)//trim(input_filehead)//"U"//trim(current_time1)//".dat"
  input1V_filename=trim(input_folder)//trim(input_filehead)//"V"//trim(current_time1)//".dat"
  input1W_filename=trim(input_folder)//trim(input_filehead)//"W"//trim(current_time1)//".dat"
  input1E_filename=trim(input_folder)//trim(input_filehead)//"E"//trim(current_time1)//".dat"
  input1T_filename=trim(input_folder)//trim(input_filehead)//"T"//trim(current_time1)//".dat"

else if (fileflag==1) then
  input1U_filename=trim(input_folder)//trim(input_filehead)//"UVWP"//trim(current_time1)//".dat"
else if  (fileflag==2) then
  input1U_filename=trim(input_folder)//trim(input_filehead)//trim(current_time1)//'.nc'
else if  (fileflag==3) then
  filenumber=start_time/1000*1000
  fileloc=mod(start_time,1000)/10
  write(current_time1,"(I07.7)") filenumber
  input1U_filename=trim(input_folder)//trim(input_filehead)//trim(current_time1)//'.dat'
end if

  xpmin=1
  xpmax=nx
  ypmin=1
  ypmax=ny
  zpmin=1
  zpmax=nz

  write(*,*)'Grid to be loaded..............'
  print*, 'x',minval(grid_x),maxval(grid_x)
  print*, 'y',minval(grid_y),maxval(grid_y)
  print*, 'z',minval(grid_z),maxval(grid_z)
  write(*,*)'Domains to be loaded..............'
  print*, 'x',xpmin,xpmax,xmin,xmax
  print*, 'y',ypmin,ypmax,ymin,ymax
  print*, 'z',zpmin,zpmax,zmin,zmax

  allocate(ENS_NORM(xpmin:xpmax))
  OPEN(0,FILE=input_ensnorm)
  do i=xpmin,xpmax
    read(0,*) ENS_NORM(i)
  end do
   CLOSE(0)
  !print*, ENS_NORM

  allocate(U1(xpmin:xpmax,ypmin:ypmax,zpmin:zpmax))
  allocate(V1(xpmin:xpmax,ypmin:ypmax,zpmin:zpmax))
  allocate(W1(xpmin:xpmax,ypmin:ypmax,zpmin:zpmax))
  allocate(VOR(xpmin:xpmax,ypmin:ypmax,zpmin:zpmax))
  allocate(DIS(xpmin:xpmax,ypmin:ypmax,zpmin:zpmax))
  !Fields needed for RK4
  allocate(U2(xpmin:xpmax,ypmin:ypmax,zpmin:zpmax))
  allocate(V2(xpmin:xpmax,ypmin:ypmax,zpmin:zpmax))
  allocate(W2(xpmin:xpmax,ypmin:ypmax,zpmin:zpmax))

  allocate(U3(xpmin:xpmax,ypmin:ypmax,zpmin:zpmax))
  allocate(V3(xpmin:xpmax,ypmin:ypmax,zpmin:zpmax))
  allocate(W3(xpmin:xpmax,ypmin:ypmax,zpmin:zpmax))

  allocate(SS(xpmin:xpmax,ypmin:ypmax,zpmin:zpmax))

if (fileflag==0) then
  call read_ens_general(input1U_filename,U3,nx,ny,nz,4)
  call read_ens_general(input1V_filename,V3,nx,ny,nz,4)
  call read_ens_general(input1W_filename,W3,nx,ny,nz,4)
  call read_ens_general(input1E_filename,VOR,nx,ny,nz,4)
  call read_ens_general(input1T_filename,SS,nx,ny,nz,4)
  SS=SS*ssnorm

else if (fileflag==1) then
  call read_ens_general(input1U_filename,UVWP1,nx*8,ny,nz,4)
  U3(:,:,:)=UVWP1(nx*0+1:nx*1,:,:)
  V3(:,:,:)=UVWP1(nx*1+1:nx*2,:,:)
  W3(:,:,:)=UVWP1(nx*2+1:nx*3,:,:)
  call compute_ens(U3,V3,W3,VOR,nx,ny,nz,dx,dy,dz)


else if (fileflag==2) then
 call read_netcdf(input1U_filename,'U',U3,0,nx-1,0,ny-1,0,nz-1)
 call read_netcdf(input1U_filename,'V',V3,0,nx-1,0,ny-1,0,nz-1)
 call read_netcdf(input1U_filename,'W',W3,0,nx-1,0,ny-1,0,nz-1)
 call read_netcdf(input1U_filename,'VOR',VOR,0,nx-1,0,ny-1,0,nz-1)
! call read_netcdf(input1U_filename,'DIS',DIS,0,nx-1,0,ny-1,0,nz-1)
else if (fileflag==3) then
  call read_binary(input1U_filename,U3,V3,W3,xpmin,xpmax,ypmin,ypmax,zpmin,zpmax,fileloc)
  call compute_vort4000(U3,V3,W3,VOR,DIS,xpmin,xpmax,ypmin,ypmax,zpmin,zpmax,dx,dy,dz)
end if

print*, 'u3',maxval(U3),minval(U3)
print*, 'v3',maxval(V3),minval(V3)
print*, 'w3',maxval(W3),minval(W3)



print*, 'ens',maxval(VOR),minval(VOR)
  !$OMP PARALLEL default(shared),private(j) num_threads(cpus)
  !$OMP DO
  do j=xpmin,xpmax
    VOR(j,:,:)=VOR(j,:,:)/ENS_NORM(j)
  end do
  !$OMP END DO
  !$OMP END PARALLEL

print*, 'ens norm',maxval(VOR),minval(VOR)

  !$OMP PARALLEL default(shared),private(j) num_threads(cpus)
  !$OMP DO

  do j=0,n-1

    call z_direction_correc(point(j,0:2),grid_z,zpmin,zpmax)

    if ((point(j,0)<grid_x(2)) .or. (point(j,0)>grid_x(nx-1))&
      .or. (point(j,1)<grid_y(2)) .or. (point(j,1)>grid_y(ny-1)) ) then
      !If particle is out of domain in this timestep, give them a flag
      point(j,7)=1
    else
      call trilinear(point(j,0:2),&
           grid_x(xpmin:xpmax),grid_y(ypmin:ypmax),grid_z(zpmin:zpmax),&
           VOR,point(j,3),xpmin,xpmax,ypmin,ypmax,zpmin,zpmax)

      if (v0flag==1) then
        point(j,4)=0.0;point(j,5)=0.0;point(j,6)=0.0
      else
        call trilinear(point(j,0:2),&
             grid_x(xpmin:xpmax),grid_y(ypmin:ypmax),grid_z(zpmin:zpmax),&
             U3,point(j,4),xpmin,xpmax,ypmin,ypmax,zpmin,zpmax)
        call trilinear(point(j,0:2),&
             grid_x(xpmin:xpmax),grid_y(ypmin:ypmax),grid_z(zpmin:zpmax),&
             V3,point(j,5),xpmin,xpmax,ypmin,ypmax,zpmin,zpmax)
        call trilinear(point(j,0:2),&
             grid_x(xpmin:xpmax),grid_y(ypmin:ypmax),grid_z(zpmin:zpmax),&
             W3,point(j,6),xpmin,xpmax,ypmin,ypmax,zpmin,zpmax)
      end if
      call trilinear(point(j,0:2),&
           grid_x(xpmin:xpmax),grid_y(ypmin:ypmax),grid_z(zpmin:zpmax),&
           SS,point(j,9),xpmin,xpmax,ypmin,ypmax,zpmin,zpmax)

    end if
  end do

  !$OMP END DO
  !$OMP END PARALLEL

allocate(max_ens(nx,0:process_files))
open(unit=0,file='max_ensN2.dat',form='unformatted',access='direct',recl=4)
lcount=1
do i=1,nx
  max_ens(i,0)=maxval(VOR(i,:,:))
  write(0,rec=lcount) max_ens(i,0)
  lcount=lcount+1
end do
close(0)

print*, output_filename 
  call create_netcdf(trim(output_filename),n)

 call write_netcdf(trim(output_filename),"x",point(:,0),n,0)
 call write_netcdf(trim(output_filename),"y",point(:,1),n,0)
 call write_netcdf(trim(output_filename),"z",point(:,2),n,0)
 call write_netcdf(trim(output_filename),"ens",point(:,3),n,0)
 call write_netcdf(trim(output_filename),"u",point(:,4),n,0)
 call write_netcdf(trim(output_filename),"v",point(:,5),n,0)
 call write_netcdf(trim(output_filename),"w",point(:,6),n,0)
 call write_netcdf(trim(output_filename),"flag",point(:,7),n,0)
 call write_netcdf(trim(output_filename),"R",point(:,8),n,0)
 call write_netcdf(trim(output_filename),"SS",point(:,9),n,0)
 call write_netcdf(trim(output_filename),"taup",point(:,10),n,0)


!**********************************************************!
!Time Loop Start Here
!**********************************************************!
t=start_time
  mean_R=0.
  count=0;countRK=0
  do i=1,process_files-1

    write(current_time1,"(I07.7)") t
    write(current_time2,"(I07.7)") t+t_interval
    write(*,*)'***********************************************'
    write(*,*)'Timestep= ',i ,' / ', process_files
    write(*,*)'Current_time= ',t
    write(*,*)'***********************************************'
    print*, 'Re initialization............'

    U1=U3
    V1=V3
    W1=W3

    xmin=minval(point(:,0))
    xmax=maxval(point(:,0))
    ymin=minval(point(:,1))
    ymax=maxval(point(:,1)) 
    zmin=minval(point(:,2))
    zmax=maxval(point(:,2))

    write(*,*)'Current location of particles..............'
    print*, 'x',xmin,xmax
    print*, 'y',ymin,ymax
    print*, 'z',zmin,zmax




    print*, 'Reading values...............'

    !Read Velocity Field at time t=t1

    if (fileflag==0) then
      input1U_filename=trim(input_folder)//trim(input_filehead)//"U"//trim(current_time1)//".dat"
      input1V_filename=trim(input_folder)//trim(input_filehead)//"V"//trim(current_time1)//".dat"
      input1W_filename=trim(input_folder)//trim(input_filehead)//"W"//trim(current_time1)//".dat"
      input1E_filename=trim(input_folder)//trim(input_filehead)//"E"//trim(current_time1)//".dat"
      input1T_filename=trim(input_folder)//trim(input_filehead)//"T"//trim(current_time1)//".dat"
      input2U_filename=trim(input_folder)//trim(input_filehead)//"U"//trim(current_time2)//".dat"
      input2V_filename=trim(input_folder)//trim(input_filehead)//"V"//trim(current_time2)//".dat"
      input2W_filename=trim(input_folder)//trim(input_filehead)//"W"//trim(current_time2)//".dat"
      input2E_filename=trim(input_folder)//trim(input_filehead)//"E"//trim(current_time2)//".dat"
      input2T_filename=trim(input_folder)//trim(input_filehead)//"T"//trim(current_time2)//".dat"
    
      !call read_ens_general(input1U_filename,U1,nx,ny,nz,4)
      !call read_ens_general(input1V_filename,V1,nx,ny,nz,4)
      !call read_ens_general(input1W_filename,W1,nx,ny,nz,4)
      call read_ens_general(input2U_filename,U3,nx,ny,nz,4)
      call read_ens_general(input2V_filename,V3,nx,ny,nz,4)
      call read_ens_general(input2W_filename,W3,nx,ny,nz,4)
    
      !Read Enstrophy at time t=t1+dt
      call read_ens_general(input2E_filename,VOR,nx,ny,nz,4)
      call read_ens_general(input2T_filename,SS,nx,ny,nz,4)
      SS=SS*ssnorm
    
    else if (fileflag==1) then
      input1U_filename=trim(input_folder)//trim(input_filehead)//'UVWP'//trim(current_time1)//'.dat'   
      input2U_filename=trim(input_folder)//trim(input_filehead)//'UVWP'//trim(current_time2)//'.dat'
      !call read_ens_general(input1U_filename,UVWP1,nx*8,ny,nz,4)
      call read_ens_general(input2U_filename,UVWP2,nx*8,ny,nz,4)
      !U1(:,:,:)=UVWP1(nx*0+1:nx*1,:,:)
      !V1(:,:,:)=UVWP1(nx*1+1:nx*2,:,:)
      !W1(:,:,:)=UVWP1(nx*2+1:nx*3,:,:)
      U3(:,:,:)=UVWP2(nx*0+1:nx*1,:,:)
      V3(:,:,:)=UVWP2(nx*1+1:nx*2,:,:)
      W3(:,:,:)=UVWP2(nx*2+1:nx*3,:,:)
      !Read Enstrophy at time t=t1+dt
      call compute_ens(U3,V3,W3,VOR,nx,ny,nz,dx,dy,dz)

    else if (fileflag==2) then
      
      input1U_filename=trim(input_folder)//trim(input_filehead)//trim(current_time1)//'.nc'
      input2U_filename=trim(input_folder)//trim(input_filehead)//trim(current_time2)//'.nc'

      !call read_netcdf(input1U_filename,'U',U1,0,nx-1,0,ny-1,0,nz-1)
      !call read_netcdf(input1U_filename,'V',V1,0,nx-1,0,ny-1,0,nz-1)
      !call read_netcdf(input1U_filename,'W',W1,0,nx-1,0,ny-1,0,nz-1)
      call read_netcdf(input2U_filename,'U',U3,0,nx-1,0,ny-1,0,nz-1)
      call read_netcdf(input2U_filename,'V',V3,0,nx-1,0,ny-1,0,nz-1)
      call read_netcdf(input2U_filename,'W',W3,0,nx-1,0,ny-1,0,nz-1)
      call read_netcdf(input1U_filename,'VOR',VOR,0,nx-1,0,ny-1,0,nz-1)
!      call read_netcdf(input1U_filename,'DIS',DIS,0,nx-1,0,ny-1,0,nz-1)
    else if (fileflag==3) then
      filenumber=t/1000*1000
      fileloc=mod(t,1000)/10
      write(current_time1,"(I07.7)") filenumber

      if (fileloc==99) then
        fileloc2=0
        write(current_time2,"(I07.7)") filenumber+1000
      else
        fileloc2=fileloc+1
        write(current_time2,"(I07.7)") filenumber
      end if
      print*,'Now at',current_time1,current_time2,fileloc,fileloc2

      input1U_filename=trim(input_folder)//trim(input_filehead)//trim(current_time1)//'.dat'
      input2U_filename=trim(input_folder)//trim(input_filehead)//trim(current_time2)//'.dat'
      !call read_binary(input1U_filename,U1,V1,W1,xpmin,xpmax,ypmin,ypmax,zpmin,zpmax,fileloc)
      call read_binary(input2U_filename,U3,V3,W3,xpmin,xpmax,ypmin,ypmax,zpmin,zpmax,fileloc2)
      call compute_vort4000(U3,V3,W3,VOR,DIS,xpmin,xpmax,ypmin,ypmax,zpmin,zpmax,dx,dy,dz)
    end if

    !open(unit=0,file='max_ensN2.dat',form='unformatted',access='direct',recl=4)
    !  do j=1,nx
    !    max_ens(j,i)=maxval(VOR(j,:,:))
    !    write(0,rec=lcount) max_ens(j,i)
    !    lcount=lcount+1
    !  end do
    !close(0)
    print*, 'skip writing max ens'

    print*, 'ens',maxval(VOR),minval(VOR)
    !$OMP PARALLEL default(shared),private(j) num_threads(cpus)
    !$OMP DO
    do j=xpmin,xpmax
      VOR(j,:,:)=VOR(j,:,:)/ENS_NORM(j)
    end do
    !$OMP END DO
    !$OMP END PARALLEL
    print*, 'ens norm',maxval(VOR),minval(VOR)


    !Linear Interpolation of field at time t=t1+dt/2 
      U2=(U1+U3)/2
      V2=(V1+V3)/2
      W2=(W1+W3)/2

    !Integration

    if (ssflag==0) print*, 'Not Using SS'
    if (ssflag/=0) print*, 'Using SS'
    if (taup==0) print*, 'Use normal RK'
    if (taup/=0) print*, 'Use inertial RK taup=', taup
    print*, 'calculating trajectory.........'
    !Calculate Trajectory
    !$OMP PARALLEL default(shared),private(j) num_threads(cpus)
    !$OMP DO


      do j=0,n-1
        !print*, j,n

        call z_direction_correc(point(j,0:2),grid_z,zpmin,zpmax)
        !print*, point(j,0:2)

        if (point(j,7)/=0) then !not conducting particle tracking when it's out of the domain
          continue

        else if ((point(j,0)<grid_x(2)) .or. (point(j,0)>grid_x(nx-1))&
            .or. (point(j,1)<grid_y(2)) .or. (point(j,1)>grid_y(ny-1))&
            .or. point(j,8)>1 ) then
            !If particle is out of domain in this timestep, give them a flag
            point_new(j,0:6)=ieee_value(point_new(j,0:6), ieee_quiet_nan)
            point_new(j,8:10)=ieee_value(point_new(j,8:10), ieee_quiet_nan)
            point(j,7)=1

        else

          if (ssflag/=0)  then
            call compute_taup(1.0,nu,point(j,8),point(j,10))
            mean_R=mean_R+point(j,8)
            count=count+1
          else
            point(j,10)=taup
          end if
            !print*, 'taup ',point(j,10)

            !Runge Kutta 4 Computation
          if (ssflag==0 .and. taup/=0) then
            if (j==0) print*, 'Inertia RK'
            call RK4_inertia_g(point(j,0:2),point(j,4:6),point_new(j,0:2),point_new(j,4:6),&
                dt,taup,U1,U2,U3,V1,V2,V3,W1,W2,W3,&
                grid_x(xpmin:xpmax),grid_y(ypmin:ypmax),grid_z(zpmin:zpmax),&
                xpmin,xpmax,ypmin,ypmax,zpmin,zpmax,2,g)

          else if (point(j,10)<=dt .or. taup==0) then
            !print*, 'Normal RK', point(j,10), dt
            countRK=countRK+1
            call RK4(point(j,0:2),dt,point_new(j,0:2),&
                U1,U2,U3,V1,V2,V3,W1,W2,W3,&
                grid_x(xpmin:xpmax),grid_y(ypmin:ypmax),grid_z(zpmin:zpmax),&
                xpmin,xpmax,ypmin,ypmax,zpmin,zpmax,point(j,7))
          else
            if (j==0) print*, 'Inertia RK different taup'
            call RK4_inertia_g(point(j,0:2),point(j,4:6),point_new(j,0:2),point_new(j,4:6),&
                dt,point(j,10),U1,U2,U3,V1,V2,V3,W1,W2,W3,&
                grid_x(xpmin:xpmax),grid_y(ypmin:ypmax),grid_z(zpmin:zpmax),&
                xpmin,xpmax,ypmin,ypmax,zpmin,zpmax,2,g)
          end if


          call z_direction_correc(point_new(j,0:2),grid_z,zpmin,zpmax)

          !print*, point(j,0:2)
          !print*, point_new(j,0:2)

          if (point(j,7)/=0) then !not conducting particle tracking when it's out of the domain
            continue

          else if ((point_new(j,0)<grid_x(1)) .or. (point_new(j,0)>grid_x(nx))&
              .or. (point_new(j,1)<grid_y(1)) .or. (point_new(j,1)>grid_y(ny))&
              .or. (isnan(point_new(j,0))) .or. (isnan(point_new(j,1)))       &
              .or. (isnan(point_new(j,1)))                                     ) then
              !If particle is out of domain in this timestep, give them a flag
              point_new(j,0:6)=ieee_value(point_new(j,0:6), ieee_quiet_nan)
              point_new(j,8:10)=ieee_value(point_new(j,8:10), ieee_quiet_nan)
              point(j,7)=1

          else

            if (point(j,10)<=dt .or. taup==0) then
              call trilinear(point_new(j,0:2),&
                   grid_x(xpmin:xpmax),grid_y(ypmin:ypmax),grid_z(zpmin:zpmax),&
                   U3,point_new(j,4),xpmin,xpmax,ypmin,ypmax,zpmin,zpmax)
              call trilinear(point_new(j,0:2),&
                   grid_x(xpmin:xpmax),grid_y(ypmin:ypmax),grid_z(zpmin:zpmax),&
                   V3,point_new(j,5),xpmin,xpmax,ypmin,ypmax,zpmin,zpmax)
              call trilinear(point_new(j,0:2),&
                   grid_x(xpmin:xpmax),grid_y(ypmin:ypmax),grid_z(zpmin:zpmax),&
                   W3,point_new(j,6),xpmin,xpmax,ypmin,ypmax,zpmin,zpmax)
            end if
            call trilinear(point_new(j,0:2),&
                 grid_x(xpmin:xpmax),grid_y(ypmin:ypmax),grid_z(zpmin:zpmax),&
                 VOR,point_new(j,3),xpmin,xpmax,ypmin,ypmax,zpmin,zpmax)
            call trilinear(point_new(j,0:2),&
                 grid_x(xpmin:xpmax),grid_y(ypmin:ypmax),grid_z(zpmin:zpmax),&
                 SS,point_new(j,9),xpmin,xpmax,ypmin,ypmax,zpmin,zpmax)
            !call droplet_growth(A3,point_new(j,9),point(j,8),point_new(j,8),dt)
            call droplet_growth(0.00005,point_new(j,9)-1,point(j,8),point_new(j,8),dt,flag)

            if (flag) point(j,7)=1

            if (ssflag/=0)  then
              call compute_taup(1.0,nu,point_new(j,8),point_new(j,10))
            else
              point_new(j,10)=taup
            end if

          end if
        end if
      end do

    !$OMP END DO
    !$OMP END PARALLEL

    print *, int(sum(point(:,7))),'/',n,'points are skipped'
    print *, countRK,'/',n,'Points are non inertial' 
    if (int(sum(point(:,7)))==n) then
      print *, 'All points are skipped. End Program.'
      exit
    end if

    print*, 'Average speed v,w[m/s] ', sum(point_new(:,5))/n
    print*, 'Average radius of particle [m] ', mean_R/count
    print*, 'Average supersaturation of particle [ND] ', sum(point_new(:,9))/n
    print*, 'Average stokes number of particle [s] ', sum(point_new(:,10))/n

    !write(*,*) 'Origin: ',point(:,:)
    !write(*,*) 'Integr: ',point_new(:,:)


    t=t+t_interval

    write(*,*)'Writing NetCDF data on ', output_filename
    call write_netcdf(trim(output_filename),"x",point_new(:,0),n,i)
    call write_netcdf(trim(output_filename),"y",point_new(:,1),n,i)
    call write_netcdf(trim(output_filename),"z",point_new(:,2),n,i)
    call write_netcdf(trim(output_filename),"ens",point_new(:,3),n,i)
    call write_netcdf(trim(output_filename),"u",point_new(:,4),n,i)
    call write_netcdf(trim(output_filename),"v",point_new(:,5),n,i)
    call write_netcdf(trim(output_filename),"w",point_new(:,6),n,i)
    call write_netcdf(trim(output_filename),"flag",point(:,7),n,i)
    call write_netcdf(trim(output_filename),"R",point_new(:,8),n,i)
    call write_netcdf(trim(output_filename),"SS",point_new(:,9),n,i)
    call write_netcdf(trim(output_filename),"taup",point_new(:,10),n,i)
    write(*,*)'Done!'
    write(*,*)''

    point(:,0:6)=point_new(:,0:6)
    point(:,8:10)=point_new(:,8:10)

  end do
  !Time Loop Ends Here
  deallocate(U1,U2,U3,V1,V2,V3,W1,W2,W3,VOR,DIS,SS)

end subroutine
