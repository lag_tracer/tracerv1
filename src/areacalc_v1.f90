subroutine areacalc_v1(n,nx,ny,nz, &
                       taup,g, &
                       grid_x, grid_y, grid_z, &
                       point_input, &
                       start_time,process_files,t_interval,dt,& 
                       input_folder,input_filehead,input_ensnorm,output_filename)

!**********************************************************************
!subroutine pytrace
!subroutine to trace particles callable from python
!Input input file as an first argument + DNS results
!Output NetCDF particle track file
!**********************************************************************

  !$ use omp_lib
  use,intrinsic::ieee_arithmetic  

  implicit none

  integer::nb_procs
  integer::i,j,k
  integer::p,q,r
  integer::l,m
  integer::t
  integer::lcount
  integer,intent(in)::nx,ny,nz

  character(len=250)::input1U_filename,input1W_filename
  character(len=250)::input1V_filename,input1E_filename
  character(len=250)::input2U_filename,input2W_filename
  character(len=250)::input2V_filename,input2E_filename
  character(len=250)::temp_filename
  character(len=*),intent(in)::output_filename
  character(len=7)  ::current_time1,current_time2
  character(len=*),intent(in) ::input_folder,input_filehead,input_ensnorm

  integer,intent(in)::n
  integer,intent(in)::start_time,process_files,t_interval
  real*4, intent(in)::dt
  real*4,dimension(0:n-1,0:2),intent(in)::point_input

  real*4,allocatable,dimension(:,:)::point,point_new
  real*4,allocatable,dimension(:,:)::max_ens
  
  !For reading UVWP
  real*4, allocatable, dimension(:,:,:)::UVWP1
  real*4, allocatable, dimension(:,:,:)::UVWP2
  
  real*4, allocatable, dimension(:,:,:)::U1,V1,W1
  real*4, allocatable, dimension(:)::Ens_Norm
  real*4, allocatable, dimension(:,:,:)::U2,V2,W2
  real*4, allocatable, dimension(:,:,:)::U3,V3,W3
  
  real*4, allocatable, dimension(:,:,:)::VOR,DIS


  real*4:: grid_x(1:nx), grid_y(1:ny), grid_z(1:nz)  
  real*4:: dx, dy, dz 

  real*4,intent(in)::taup,g

  !Areacalc
  real*8, allocatable, dimension(:)::area_count
  real*8::threshold

  real*4::xmin,xmax,ymin,ymax,zmin,zmax
  integer::xstart,ystart,xpmin,xpmax,ypmin,ypmax,zpmin,zpmax

!$OMP PARALLEL
!$  nb_procs = OMP_GET_NUM_THREADS()
!$OMP END PARALLEL
      write(*,*)'---------------------------'
      write(*,*)'Number of processors :',nb_procs
      write(*,*)'---------------------------'

    write(*,*)'***********************************************'
    write(*,*)'Timestep= ',0
    write(*,*)'Current_time= ',start_time 
    write(*,*)'***********************************************'

print*, 'Input information from Python'
    write(*,*)'***********************************************'
print*, 'n nx ny nz', n, nx, ny, nz
print*, 'taup g', taup,g
print*, 'point input x min max', minval(point_input(:,0)), maxval(point_input(:,0))
print*, 'point input y min max', minval(point_input(:,1)), maxval(point_input(:,1))
print*, 'point input z min max', minval(point_input(:,2)), maxval(point_input(:,2))
print*, 'start_time', start_time
print*, 'process_files',process_files
print*, 't_interval',t_interval
print*, 'dt',dt
print*, 'input_folder ',input_folder
print*, 'input_filehead ',input_filehead
print*, 'input_ensnorm ',input_ensnorm
print*, 'output_filename ',output_filename

write(*,*)'***********************************************'

dx=grid_x(2)-grid_x(1)
dy=grid_y(2)-grid_y(1)
dz=grid_z(2)-grid_z(1)

print*, 'dx dy dz = ', dx,dy,dz

allocate(point(0:n-1,0:7),point_new(0:n-1,0:7))
allocate(UVWP1(nx*8,ny,nz))
allocate(UVWP2(nx*8,ny,nz))

point(:,7)=0
!Copy input points
point(:,0)=point_input(:,0)
point(:,1)=point_input(:,1)
point(:,2)=point_input(:,2)

xmin=minval(point(:,0))
xmax=maxval(point(:,0))
ymin=minval(point(:,1))
ymax=maxval(point(:,1)) 
zmin=minval(point(:,2))
zmax=maxval(point(:,2))

print*, 'dt=',dt

!Read Entire Grid
write(current_time1,"(I07.7)") start_time

input1U_filename=trim(input_folder)//trim(input_filehead)//"UVWP"//trim(current_time1)//".dat"

xpmin=1
xpmax=nx
ypmin=1
ypmax=ny
zpmin=1
zpmax=nz

  write(*,*)'Grid to be loaded..............'
  print*, 'x',minval(grid_x),maxval(grid_x)
  print*, 'y',minval(grid_y),maxval(grid_y)
  print*, 'z',minval(grid_z),maxval(grid_z)
  write(*,*)'Domains to be loaded..............'
  print*, 'x',xpmin,xpmax,xmin,xmax
  print*, 'y',ypmin,ypmax,ymin,ymax
  print*, 'z',zpmin,zpmax,zmin,zmax

  allocate(ENS_NORM(xpmin:xpmax))
  OPEN(0,FILE=input_ensnorm)
  do i=xpmin,xpmax
    read(0,*) ENS_NORM(i)
  end do
   CLOSE(0)
  print*, ENS_NORM

  allocate(U3(xpmin:xpmax,ypmin:ypmax,zpmin:zpmax))
  allocate(V3(xpmin:xpmax,ypmin:ypmax,zpmin:zpmax))
  allocate(W3(xpmin:xpmax,ypmin:ypmax,zpmin:zpmax))

  allocate(VOR(xpmin:xpmax,ypmin:ypmax,zpmin:zpmax))

call read_ens_general(input1U_filename,UVWP1,nx*8,ny,nz,4)

U3(:,:,:)=UVWP1(nx*0+1:nx*1,:,:)
V3(:,:,:)=UVWP1(nx*1+1:nx*2,:,:)
W3(:,:,:)=UVWP1(nx*2+1:nx*3,:,:)

print*, 'u3',maxval(U3),minval(U3)
print*, 'v3',maxval(V3),minval(V3)
print*, 'w3',maxval(W3),minval(W3)

call compute_ens(U3,V3,W3,VOR,nx,ny,nz,dx,dy,dz)

!print*, 'ens',maxval(VOR),minval(VOR)
!  !$OMP PARALLEL default(shared),private(j) num_threads(48)
!  !$OMP DO
!  do j=xpmin,xpmax
!    VOR(j,:,:)=VOR(j,:,:)/ENS_NORM(j)
!  end do
!  !$OMP END DO
!  !$OMP END PARALLEL
!
!print*, 'ens norm',maxval(VOR),minval(VOR)

allocate(area_count(xpmin:xpmax))

!Loop for different thresholds
open(unit=202,file='Area.txt')
do i=1,200
  threshold=1000.d0/1.2d0**(i)
  print*, i,threshold
  !Main function calculates area (area_count) from VOR(nx,ny,nz)
  !I think i'm throwing some unusable variables so please take a look at areacalc_slice and clean if necessary
  call areacalc_slice(VOR,xpmin,xpmax,ypmin,ypmax,zpmin,zpmax,dx,dy,dz,threshold,area_count,grid_y)
  write(202,*) threshold,area_count
end do
close(202)


end subroutine


subroutine areacalc_slice(VOR,xpmin,xpmax,ypmin,ypmax,zpmin,zpmax,dx,dy,dz,threshold,areacount,grid_y)

!**********************************************************************
!Subroutine areacalc_slice
!Subroutine to calculate area within the threshold for each x slices (y-z plane)
!**********************************************************************
  
  !$ use omp_lib
  Implicit none  
  Integer i,j,k,l

  integer,intent(in)::xpmin,xpmax,ypmin,ypmax,zpmin,zpmax
  real, dimension(xpmin:xpmax,ypmin:ypmax,zpmin:zpmax)::VOR
  real, dimension(ypmin:ypmax)::grid_y
  double precision, dimension(xpmin:xpmax)::areacount
  real::dx,dy,dz
  double precision,intent(in)::threshold
  double precision::dudx,dudy,dudz,dvdx,dvdy,dvdz,dwdx,dwdy,dwdz


areacount=0.d0

!Using openmp to make it quick!
!$OMP PARALLEL default(shared),private(i,j,k)
!$OMP DO reduction(+:areacount)
  do i=xpmin,xpmax
    do j=ypmin,ypmax
      do k=zpmin,zpmax
        
        if (VOR(i,j,k)>threshold) then
          if (j/=ypmax) then
            areacount(i)=areacount(i)+(grid_y(j+1)-grid_y(j))
          else
            areacount(i)=areacount(i)+(grid_y(j)-grid_y(j-1))
          end if
        end if

        if (areacount(i)<0.d0) then
          print*,'area count negative ijkgrid', i,j,k,grid_y(j-1),grid_y(j),grid_y(j+1)
          areacount(i)=0
        end if

      end do
    end do
  end do
!$OMP END DO
!$OMP END PARALLEL
  return
end subroutine

subroutine areacalc(VOR,xpmin,xpmax,ypmin,ypmax,zpmin,zpmax,dx,dy,dz,threshold,count)

!**********************************************************************
!Subroutine compute_vorticity
!Subroutine to compute vorticity of the field
!Input module Definition
!Output module Definition
!**********************************************************************
  
  !$ use omp_lib
  Implicit none  
  Integer i,j,k,l

  integer,intent(in)::xpmin,xpmax,ypmin,ypmax,zpmin,zpmax
  real, dimension(xpmin:xpmax,ypmin:ypmax,zpmin:zpmax)::VOR
  real::dx,dy,dz
  double precision,intent(in)::threshold
  integer::count
  double precision::dudx,dudy,dudz,dvdx,dvdy,dvdz,dwdx,dwdy,dwdz


count=0
!$OMP PARALLEL default(shared),private(i,j,k,dudx,dudy,dudz,dvdx,dvdy,dvdz,dwdx,dwdy,dwdz)
!$OMP DO reduction(+:count)
  do i=xpmin,xpmax
    do j=ypmin,ypmax
      do k=zpmin,zpmax
        
        if (VOR(i,j,k)>threshold) then
          count=count+1
        end if
      end do
    end do
  end do
!$OMP END DO
!$OMP END PARALLEL
  return
end subroutine
