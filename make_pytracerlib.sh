gfortran -c src/droplet.f90
gfortran -O3 -shared -fPIC -fopenmp -o lib/pytracer_v1.so src/pytracer_v1.f90 src/droplet.f90 src/netcdflib.f90 src/pytracerlib.f90 src/find_point.f90 -mcmodel=medium -I/usr/local/include -L/usr/local/lib  -lnetcdff -lnetcdf
